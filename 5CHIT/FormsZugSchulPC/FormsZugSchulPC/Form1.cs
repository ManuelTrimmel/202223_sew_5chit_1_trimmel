namespace FormsZugSchulPC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        TrainModel trainModel;
        TrainView trainView;
        TrainController trainController;

        private void Form1_Load(object sender, EventArgs e)
        {

            trainModel = new TrainModel();
            trainView = new TrainView(trainModel, label1,this);
            trainController = new TrainController(trainModel, trainView);
            trainModel.HasMoved += trainView.Model_HasMoved;
    
        }

        private void button1_Click(object sender, EventArgs e)
        {
            trainController.Drive(50);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            trainController.Drive(200);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            trainController.Drive();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label2.Text= trackBar1.Value.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            trainController.Drive(trackBar1.Value*20);
        }
    }



    public class TrainModel
    {
        int lastdisplay;
        public int Pos { get; set; }
        public event EventHandler? HasMoved;
        public event EventHandler? IsLost;
        public void Move(int up = 10)
        {
            Pos += up;
            if (Pos - lastdisplay > 50)
                if (HasMoved != null)
                {
                    lastdisplay = Pos;
                    HasMoved(this, new TrainSpeedEventargs { CurrentsSpeed = up });
                }
            if (IsLost != null)
            {
                IsLost(this, new TrainOutOfBoundsArgs { CurrentPosition = Pos });
                //Pos = 0;
                lastdisplay = 0;
            }
        }

    }

    public class TrainController
    {
        public TrainModel model { get; init; }
        TrainView view;
        public TrainController(TrainModel model, TrainView view)
        {
            this.model = model;
            this.view = view;
        }
        public void Drive(int up = 20)
        {
            model.Move(up);
        }
    }

    public class TrainView
    {
        public TrainModel model;
        public Form form { get; private set; }
        public Label label { get; private set; }
        public TrainView(TrainModel model, Label label, Form form)
        {
            this.model = model;
            this.form = form;
            this.label = label;
            
            //model.HasMoved += Model_HasMoved;
        }

        public void Model_HasMoved(object? sender, EventArgs e)
        {
            int speed = (e as TrainSpeedEventargs).CurrentsSpeed;

            if (speed < 50)
                label.ForeColor = Color.Green;
            //Console.ForegroundColor = ConsoleColor.Green;
            if (speed >= 50)
                label.ForeColor = Color.Yellow;
            //Console.ForegroundColor = ConsoleColor.Yellow;
            if (speed >= 150)
                label.ForeColor = Color.Red;
            //Console.ForegroundColor = ConsoleColor.Red;
            if (model.Pos >= form.Width || model.Pos <= 0)
            {
                model.IsLost += Model_IsLost;
            }

            
            label.Left = model.Pos;
            

            label.Top = 30;
            form.Update();
            //Console.Clear();
            //Console.SetCursorPosition(model.Pos / 100, 0);
            //Console.WriteLine("Train");
        }
        public void Model_IsLost(object? sender, EventArgs e)
        {
            int position = (e as TrainOutOfBoundsArgs).CurrentPosition;
            if (position == 0)
                model.Pos = form.Size.Width-20;
            if (position >= form.Size.Width)
                model.Pos = 10;
            else
                model.Pos = position;
        }


    }
    public class TrainSpeedEventargs : EventArgs
    {
        public int CurrentsSpeed { get; init; }
    }
    public class TrainOutOfBoundsArgs : EventArgs
    {
        public int CurrentPosition { get; init; }  
    }

}