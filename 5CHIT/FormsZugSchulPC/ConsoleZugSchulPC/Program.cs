﻿// See https://aka.ms/new-console-template for more information

TrainModel tm = new TrainModel();
TrainView v = new TrainView(tm);
TrainController c = new TrainController(tm, v);
tm.HasMoved += v.Model_HasMoved;

//BadTrain c = new BadTrain();

c.Drive(); System.Threading.Thread.Sleep(333);
c.Drive(50); System.Threading.Thread.Sleep(333);
c.Drive(50); System.Threading.Thread.Sleep(333);
c.Drive(50); System.Threading.Thread.Sleep(333);
c.Drive(100); System.Threading.Thread.Sleep(333);
c.Drive(150); System.Threading.Thread.Sleep(333);
c.Drive(200); System.Threading.Thread.Sleep(333);
c.Drive(10); System.Threading.Thread.Sleep(333);
c.Drive(200); System.Threading.Thread.Sleep(333);
c.Drive(150); System.Threading.Thread.Sleep(333);
c.Drive(100); System.Threading.Thread.Sleep(333);
c.Drive(10); System.Threading.Thread.Sleep(333);
c.Drive(50); System.Threading.Thread.Sleep(333);
c.Drive(50); System.Threading.Thread.Sleep(333);
c.Drive(); System.Threading.Thread.Sleep(333);
class BadTrain
{
    int lastdisplay;
    int pos;
    public void Drive(int up = 10)
    {
        Console.ForegroundColor = ConsoleColor.White;
        pos += up;
        if (up < 50)
            Console.ForegroundColor = ConsoleColor.Green;
        if (up >= 50)
            Console.ForegroundColor = ConsoleColor.Yellow;
        if (up >= 150)
            Console.ForegroundColor = ConsoleColor.Red;

        if (pos - lastdisplay > 500 || pos == 0)
            Display();      //show only if starting, or it has moved for more than 50 units
    }

    public void Display()
    {
        lastdisplay = pos;
        Console.Clear();
        Console.SetCursorPosition(pos / 100, 0);
        Console.WriteLine("Train");
    }
}

public class TrainModel
{
    int lastdisplay;
    public int Pos { get; private set; }
    public event EventHandler? HasMoved;
    public void Move(int up = 10)
    {
        Pos += up;
        if (Pos - lastdisplay > 500)
            if (HasMoved != null)
            {
                lastdisplay = Pos;
                HasMoved(this, new TrainSpeedEventargs { CurrentsSpeed = up });
            }
    }
}

public class TrainController
{
    public TrainModel model { get; init; }
    TrainView view;
    public TrainController(TrainModel model, TrainView view)
    {
        this.model = model;
        this.view = view;
    }
    public void Drive(int up = 20)
    {
        model.Move(up);
    }
}

public class TrainView
{
    public TrainModel model;
    public TrainView(TrainModel model)
    {
        this.model = model;
        //model.HasMoved += Model_HasMoved;
    }

    public void Model_HasMoved(object? sender, EventArgs e)
    {
        int speed = (e as TrainSpeedEventargs).CurrentsSpeed;

        if (speed < 50)
            Console.ForegroundColor = ConsoleColor.Green;
        if (speed >= 50)
            Console.ForegroundColor = ConsoleColor.Yellow;
        if (speed >= 150)
            Console.ForegroundColor = ConsoleColor.Red;

        Console.Clear();
        Console.SetCursorPosition(model.Pos / 100, 0);
        Console.WriteLine("Train");
    }
}
public class TrainSpeedEventargs : EventArgs
{
    public int CurrentsSpeed { get; init; }
}

