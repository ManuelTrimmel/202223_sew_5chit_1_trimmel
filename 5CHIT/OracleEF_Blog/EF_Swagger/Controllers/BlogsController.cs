﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OracleEF_Blog.Model;

namespace EF_Swagger.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BlogsController : GenericController<Blog>
    {
        public BlogsController() { }
    }
}
