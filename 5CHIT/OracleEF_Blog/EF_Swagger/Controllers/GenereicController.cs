﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OracleEF_Blog.Model;

namespace EF_Swagger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T> : ControllerBase where T : class
    {
        BloggingContext context = new BloggingContext();
        [EnableCors()]
        // GET: api/get
        [HttpGet]
        public IEnumerable<T> Get()
        {
            var entity = context.Set<T>();
            return entity;
        }

        // GET api/5
        [EnableCors()]
        [HttpGet("{id}")]
        public T Get(int id)
        {
            var entity = context.Set<T>().Find(id);
            return entity;
        }

        // POST api/
        [HttpPost]
        public void Post([FromBody] T value)
        {
            context.Set<T>().Add(value);
            context.SaveChanges();
        }

        // PUT api/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] T value)
        { 
            if (context.Set<T>().Contains(value) == true)
            {
                context.Set<T>().Update(value);
            }
            context.SaveChanges();

        }

        // DELETE api//5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                context.Set<T>().Remove(Get(id));
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("geht ned");
            }
        }
    }
}
