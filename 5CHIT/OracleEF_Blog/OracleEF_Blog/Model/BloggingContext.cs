﻿using Microsoft.EntityFrameworkCore;
using OracleEF_Blog;


namespace OracleEF_Blog.Model
{
    public class BloggingContext: DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseOracle("User Id = c##trimmel; Password = trimmel; Data Source = localhost:1521 / ORCLCDB;");
        }
    }

}
