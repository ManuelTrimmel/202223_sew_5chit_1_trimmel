﻿using OracleEF_Blog.Migrations;
using OracleEF_Blog.Model;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

using (var db = new BloggingContext())
{
    //anlegen
    db.RemoveRange(db.Blogs);
    db.RemoveRange(db.Posts);
    db.Blogs.Add(new Blog { Id = 1, Name = "Blog1" });
    db.Blogs.Add(new Blog { Id = 2, Name = "Blog2" });
    Blog blog = db.Blogs.Where(b => b.Id == 1).First();
    Blog blog2 = db.Blogs.Where(b => b.Id == 2).First();

    db.Posts.Add(new Post { Id = 1, Title = "Post1", Content = "Lorem Ipsum Mehrweg Hacho", Blog = blog, DateCreated = DateTime.Now });
    db.Posts.Add(new Post { Id = 2, Title = "Post2", Content = "Lorem Ipsum Mehrweg Hacho2", Blog = blog, DateCreated = DateTime.Now });
    db.Posts.Add(new Post { Id = 3, Title = "Post3", Content = "Mehrweg Hacho", Blog = blog2, DateCreated = DateTime.Now });
    db.Posts.Add(new Post { Id = 4, Title = "Post4", Content = "Lorem Hacho2", Blog = blog2, DateCreated = DateTime.Now });
    db.SaveChanges();
    //auslesen
    var blogs = db.Blogs.ToList();

    var posts = db.Posts.ToList();

    //In der PaketmanagerConsole: 
    //dotnet ef migrations add InitialCreate
    //dotnet ef database update


    //anzeigen
    foreach (var item in blogs)
    {
        Console.WriteLine($"{item.Id}:{item.Name}");
    }
    Console.WriteLine("-----------------------------");
    foreach(var item in posts)
    {
        Console.WriteLine($"{item.Id}: {item.Title}: {item.Blog.Name}");
    }
}