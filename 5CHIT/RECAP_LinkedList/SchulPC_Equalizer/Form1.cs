namespace SchulPC_Equalizer
{
    public partial class Form1 : Form
    {
        Random r;
        int anzahl;
        int[] ywerte;
        Panel[] panelArray;
        Label[] labelArray;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            anzahl = 10;
            ywerte = new int[anzahl];
            panelArray = new Panel[anzahl];
            labelArray = new Label[anzahl];
            for (int i = 0; i < anzahl; i++)
            {
                panelArray[i] = new Panel();
                labelArray[i] = new Label();
            }
            timer1.Interval = 100;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           // Controls.Clear();
            getRandom();
            drawbox();
        }

        public void drawbox()
        {
            for (int i = 0; i < anzahl; i++)
            {
                Panel panel = panelArray[i];
                panel.BackColor = Color.Red;
                panel.Size = new Size(55, ywerte[i]);
                panel.Top = this.ClientSize.Height - 150 - ywerte[i];
                panel.Left = 80 * i + 20;
                Label label = labelArray[i];
                label.Text = ywerte[i].ToString();
                label.Top = 320;
                label.Left = 80 * i + 20;
                label.Width = 80;
                this.Controls.Add(label);
                this.Controls.Add(panel);
                
            }
           
        }

        public void getRandom()
        {
            r = new Random();
            for (int i = 0; i < anzahl; i++)
            {
                ywerte[i] = r.Next(50, 200);
            }
        }
    }
}