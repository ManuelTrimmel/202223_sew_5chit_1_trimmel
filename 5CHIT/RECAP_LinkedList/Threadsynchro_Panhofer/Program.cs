﻿Thread machineA = new Thread(() => MachineA.Run());

Thread machineB = new Thread(() => MachineB.Run());

Thread conveyer = new Thread(() => ConveyerBelt.Run());


machineA.Start();
machineB.Start();
conveyer.Start();



public class MachineA
{
    public static void Run()
    {
        while (true)
        {
            Crane.machinea.WaitOne();
            Process();
            ConveyerBelt.conveyer.Release();
        }
    }

    public static void Process()
    {
        Thread.Sleep(200);
        Console.WriteLine("MaschineA: finished process");
    }

}


public class MachineB
{
    public static void Run()
    {
        while (true)
        {
            Crane.machineb.WaitOne();
            Process();
            ConveyerBelt.conveyer.Release();
        }
    }

    public static void Process()
    {
        Thread.Sleep(200);
        Console.WriteLine("MaschineB: finished process");
    }

}

public class Crane
{
    public static Semaphore machinea = new Semaphore(0, 1);
    public static Semaphore machineb = new Semaphore(0, 1);
    public static Semaphore crane = new Semaphore(0, 1);

    public static void Run()
    {
        while (true)
        {
            
            Move("Storage", "MachineA");
            machinea.Release();

            Move("MachineA", "MachineB");
            machineb.Release();

            Move("MachineB", "Storage");
        }
    }

    public static void Move(string from, string to)
    {
        Thread.Sleep(300);
        Console.WriteLine($"moving from {from} to {to}");
    }
}

public class ConveyerBelt
{
    public static Semaphore conveyer = new Semaphore(2, 2);

    public static void Run()
    {
        while (true)
        {
            conveyer.WaitOne();
            conveyer.WaitOne();
            Move();
            Crane.machinea.Release();
            Crane.machineb.Release();
        }
    }

    public static void Move()
    {
        Thread.Sleep(200);
        Console.WriteLine("Conveyer Belt Is Turning");
    }
}
