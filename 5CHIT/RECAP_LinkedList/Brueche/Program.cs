﻿using Brueche;

myval b1 = new myval(1, 3);
myval b2 = new myval(1, 4);
myval b3 = b1 + b2;
myval b4 = b1 * b2;
myval b5 = b1 / b3;
myval b6 = new myval(2, 4);

myval b7 = new myval(6, 12);
myval b8 = b6 * b7;
myval b9 = new myval(1, 3);
//Console.WriteLine(b3);
Console.WriteLine(-b1);
Console.WriteLine(b4);
Console.WriteLine(b5);
Console.WriteLine(b6);
Console.WriteLine(-b6);
Console.WriteLine(b8);

Console.WriteLine(b1 == b9);
Console.WriteLine(b1 == b2);