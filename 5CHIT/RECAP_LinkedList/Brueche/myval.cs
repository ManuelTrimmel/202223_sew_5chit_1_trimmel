﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Brueche
{
    internal class myval
    {
        int divisor;
        int divident;
        public myval(int divident, int divisor)
        {
            int ggt = euklid(divident, divisor);

            if (divisor < 0)
            {
                this.divident = -divident / ggt;
                this.divisor = -divisor / ggt;
            }
            else
            {
                this.divident = divident / ggt;
                this.divisor = divisor / ggt;
            }

        }

        static int euklid(int a, int b)
        {
            if (a < 0) a = -a;
            if (b < 0) b = -b;

            if (b == 0)
            {
                return a;
            }
            else if (a == 0)
            {
                return b;
            }
            else if (a > b)
            {
                return euklid(a - b, b);
            }
            else
            {
                return euklid(a, b - a);
            }
        }

        public static myval operator +(myval a) => a;
        public static myval operator -(myval a) => new myval(-a.divident, a.divisor);

        public static myval operator +(myval a, myval b)
        => new myval(a.divident * b.divisor + b.divident * a.divisor, a.divisor * b.divisor);

        public static myval operator -(myval a, myval b)
        => a + (-b);


        public static myval operator *(myval a, myval b)
            => new myval(a.divident * b.divident, a.divisor * b.divisor);


        public static myval operator /(myval a, myval b)
        {
            if (b.divident == 0)
            {
                throw new DivideByZeroException();
            }
            return new myval(a.divident * b.divisor, a.divisor * b.divident);
        }
        public static bool operator ==(myval a, myval b)
        {
            //a.equals(b)
            return a.divident == b.divident && a.divisor == b.divisor;
        }
        public static bool operator !=(myval a, myval b)
        {  
            return !(a.divident == b.divident && a.divisor == b.divisor)ö;
        }
    
//public static myval kuerzen(myval erg)
//{
//    myval kurz = new myval(0, 0);
//    int ggT = 1;
//    int z = erg.divident;
//    float n = erg.divisor;

//    if (z > n)
//        ggT = erg.divident % erg.divisor;
//    else
//        ggT = erg.divisor % erg.divident;

//    kurz.divident = (erg.divident / ggT);
//    kurz.divisor = (erg.divisor / ggT);
//    return kurz;
//}

public override string ToString()
        {
            return $"{divident} / {divisor}";
        }



    }

}
