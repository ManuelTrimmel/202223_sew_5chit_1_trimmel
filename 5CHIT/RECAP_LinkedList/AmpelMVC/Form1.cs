namespace AmpelMVC
{
    public partial class Form1 : Form
    {
        AmpelController controller;
        AmpelModel model;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            model = new AmpelModel();
            controller = new AmpelController(model, this);
        }
        
        public void UpdateView(object sender, EventArgs e)
        {
            if (model.isGreen)
                boxgreen.BackColor = Color.Green;
            else boxgreen.BackColor = Color.Gray;

            if(model.isYellow)
                boxyellow.BackColor = Color.Yellow;
            else boxyellow.BackColor = Color.Gray;

            if (model.isRed)
                boxred.BackColor = Color.Red;
            else boxred.BackColor = Color.Gray;

            Update();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            model.NotifyAmpelFinished();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }
    }
}