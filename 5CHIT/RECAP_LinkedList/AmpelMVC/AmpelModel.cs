﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmpelMVC
{
    internal class AmpelModel
    {
        public bool isGreen { get;  set; }

        public bool isYellow { get;  set; }

        public bool isRed { get;  set; }

        public async Task goGreen()
        {
            isGreen = true;
            isYellow = false;
            isRed = false;
            if (AmpelChanged != null) { NotifyAmpelChanged(); }
            await Task.Delay(1000);
            //if (AmpelFinished != null) { NotifyAmpelFinished(); }
        }

        public async Task goYellow()
        {
            isYellow = true;
            isRed = false;
            isGreen = false;
            if (AmpelChanged != null) { NotifyAmpelChanged(); }
            await Task.Delay(1000);
            //if (AmpelFinished != null) { NotifyAmpelFinished(); }
            
        }
        public async Task goRed()
        {
            isRed = true;
            isGreen= false;
            isYellow = false;
            if (AmpelChanged != null) { NotifyAmpelChanged(); }
            await Task.Delay(1000);
            //if (AmpelFinished != null) { NotifyAmpelFinished(); }
        }
        public async Task goRedYellow()
        {
            isRed = true;
            isYellow = true;
            isGreen= false;
            if (AmpelChanged != null) { NotifyAmpelChanged(); }
            await Task.Delay(1000);
            //if (AmpelFinished != null) { NotifyAmpelFinished(); }
        }
        public async Task goBlinkGreen()
        {
            for (int i = 0; i < 4; i++)
            {
                isGreen = false;
                await Task.Delay(1000);
                if (AmpelChanged != null) { NotifyAmpelChanged(); }
                isGreen = true;
                await Task.Delay(1000);
                if (AmpelChanged != null) { NotifyAmpelChanged(); }
            }
            isGreen= false;
            if (AmpelChanged != null) { NotifyAmpelChanged(); }
            //if (AmpelFinished != null) { NotifyAmpelFinished(); }

        }
        public event EventHandler AmpelChanged;
        public event EventHandler AmpelFinished;
        public void NotifyAmpelChanged()
        {
            AmpelChanged(this, EventArgs.Empty);
        }
        public void NotifyAmpelFinished()
        {
            AmpelFinished(this, EventArgs.Empty);
        }
    }
}
