﻿namespace AmpelMVC
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.boxred = new System.Windows.Forms.PictureBox();
            this.boxyellow = new System.Windows.Forms.PictureBox();
            this.boxgreen = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.boxred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxyellow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxgreen)).BeginInit();
            this.SuspendLayout();
            // 
            // boxred
            // 
            this.boxred.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.boxred.Location = new System.Drawing.Point(217, 41);
            this.boxred.Name = "boxred";
            this.boxred.Size = new System.Drawing.Size(100, 50);
            this.boxred.TabIndex = 0;
            this.boxred.TabStop = false;
            // 
            // boxyellow
            // 
            this.boxyellow.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.boxyellow.Location = new System.Drawing.Point(217, 111);
            this.boxyellow.Name = "boxyellow";
            this.boxyellow.Size = new System.Drawing.Size(100, 50);
            this.boxyellow.TabIndex = 1;
            this.boxyellow.TabStop = false;
            // 
            // boxgreen
            // 
            this.boxgreen.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.boxgreen.Location = new System.Drawing.Point(217, 182);
            this.boxgreen.Name = "boxgreen";
            this.boxgreen.Size = new System.Drawing.Size(100, 50);
            this.boxgreen.TabIndex = 2;
            this.boxgreen.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(476, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(476, 111);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.boxgreen);
            this.Controls.Add(this.boxyellow);
            this.Controls.Add(this.boxred);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.boxred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxyellow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxgreen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox boxred;
        private PictureBox boxyellow;
        private PictureBox boxgreen;
        private Button button1;
        private Button button2;
        private System.Windows.Forms.Timer timer1;
    }
}