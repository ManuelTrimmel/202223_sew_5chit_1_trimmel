﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.AxHost;

namespace AmpelMVC
{
    internal class AmpelController
    {
        AmpelModel model;
        Form1 view;
        private int state = 0;
        public AmpelController(AmpelModel model, Form1 view)
        {
            this.model = model;
            this.view = view;
            model.AmpelChanged += view.UpdateView;
            model.AmpelFinished += ChangeState;

        }

        public async void ChangeState(object sender, EventArgs e)
        {
                state++;
                switch (state)
                {
                    case 1: await model.goGreen(); break;
                    case 2: await model.goBlinkGreen(); break;
                    case 3: await model.goYellow(); break;
                    case 4: await model.goRed(); break;
                    case 5: await model.goRedYellow(); break;
                    default: state = 0; break;
                }
                model.NotifyAmpelFinished();
        }
    }
}
