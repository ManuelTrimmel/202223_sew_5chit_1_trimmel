﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBallMVC
{
    public class BallModel
    {
        public double X { get; set; }
        public double Y { get; set; }

        public double deltax = 5.67;
        public double deltay = 5.13;


        public event EventHandler BallHasMoved;

        public void NotifyBallHasMoved()
        {
            BallHasMoved(this, EventArgs.Empty);
        }
    }
}
