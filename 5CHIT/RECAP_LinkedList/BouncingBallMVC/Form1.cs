namespace BouncingBallMVC
{
    public partial class Form1 : Form
    {
        BallController controller;
        BallModel model;
        BallView view;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            model = new BallModel();
            view = new BallView(this, panel1,model);
            controller = new BallController(model, view);
            timer1.Start();
            
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            controller.Move();
        }
    }
}