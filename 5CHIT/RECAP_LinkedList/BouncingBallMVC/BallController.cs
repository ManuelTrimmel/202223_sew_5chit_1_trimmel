﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBallMVC
{
    public class BallController
    {
        BallModel model;
        BallView view;
        public BallController (BallModel model, BallView view)
        {
            this.model = model;
            this.view = view;
            //Die ganzen Events gleich im Konstruktor subscriben
            model.BallHasMoved += view.Draw;
            view.BounceCheckX += TurnX;
            view.BounceCheckY += TurnY;
        }
        public void Move()
        {
            model.X += model.deltax; model.Y += model.deltay;
            model.NotifyBallHasMoved();
            //Sobald sich der Ball bewegt, wird überprüft, ob er Bouncen kann
            CheckBounce();
        }
        public void TurnX(object sender, EventArgs e) { model.deltax = -model.deltax; model.NotifyBallHasMoved(); }
        public void TurnY(object sender, EventArgs e) { model.deltay = -model.deltay; model.NotifyBallHasMoved(); }


        //Controller überprüft die Werte des Models mit den Daten der View und schaut ob es Bouncen kann. Kann es nun Bouncen, ruft der Controller
        //die NotifyBounceCheck aus der View auf, weil die View so wenig Logik wie nur möglich beinhalten soll
        public void CheckBounce()
        {
            if (model.X < 0) view.NotifyBounceCheckX();
            if (model.X + view.p.Width > view.f.ClientSize.Width) view.NotifyBounceCheckX(); ;
            if (model.Y < 0) view.NotifyBounceCheckY();
            if (model.Y + view.p.Height > view.f.ClientSize.Height) view.NotifyBounceCheckY();

        }

    }
}
