﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBallMVC
{
    public class BallView
    {
        public Form f { get; init; }
        public Panel p { get; init; }

        public BallModel model;
        public BallView(Form f, Panel p, BallModel model)
        {
            this.f = f;
            this.p = p;
            this.model = model;
        }

        public void Draw(object sender, EventArgs e)
        {
            p.Top = (int)model.Y;
            p.Left = (int)model.X;
        }

        public event EventHandler BounceCheckY;
        public event EventHandler BounceCheckX;

        public void NotifyBounceCheckX()
        {
            BounceCheckX(this, EventArgs.Empty);
        }
        public void NotifyBounceCheckY()
        {
            BounceCheckY(this, EventArgs.Empty);
        }
    }
}
