﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBall
{
    public abstract class BasicItem
    {
        public Form f { get; init; }
        public Panel p { get; init; }
        public double X { get; set; }
        public double Y { get; set; }
        public double deltax = 5.67;
        public double deltay = 5.13;
        public abstract void Move();
        public abstract void CheckBounce();
        public abstract void TurnX();
        public abstract void TurnY();
    }
}
