﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBall
{
    public class Ball : BasicItem
    {
        public override void Move()
        {
            X += deltax; Y += deltay;
        }
        public override void CheckBounce()
        {
            if (X < 0) TurnX();
            if (X + p.Width > f.ClientSize.Width) TurnX();
            if (Y < 0) TurnY();
            if (Y + p.Height > f.ClientSize.Height) TurnY();
        }
        public override void TurnX() { deltax = -deltax; }
        public override void TurnY() { deltay = -deltay; }
    }
}
