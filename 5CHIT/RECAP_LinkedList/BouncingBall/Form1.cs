namespace BouncingBall
{
    public partial class Form1 : Form
    {
        Ball b;

        Item i;
        public Form1()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            b = new Ball
            {
                f = this,
                p = this.panel1
            };
            i = new Item
            {
                f = this,
                p = this.panel2
            };
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            b.CheckBounce();
            b.Move();

            this.panel1.Top = (int)b.Y;
            this.panel1.Left = (int)b.X;

            this.panel2.Top = (int)i.Y;
            this.panel2.Left = (int)i.X;

            i.Move();
            i.CheckBounce();

            if ((b.Y < i.Y && b.X < i.X))
            {
                if (b.Y + b.p.Height > i.Y && b.X + b.p.Width > i.X)
                {
                    i.TurnX();
                    i.TurnY();
                }

            }
        }
    }
}