﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ_CrashCourse
{
    internal class Employee
    {
        public Employee(int id, string eNAME, string jOB, int parent_id, DateOnly hIREDATE, int sAL, int cOMM, int dept_id)
        {
            this.id = id;
            ENAME = eNAME;
            JOB = jOB;
            this.parent_id = parent_id;
            

            
            HIREDATE = hIREDATE;
            SAL = sAL;
            COMM = cOMM;
            this.dept_id = dept_id;
        }

        private int id { get; set; }
        private string ENAME { get; set; }
        private string JOB { get; set; }
        private int? parent_id { get; set; }
        private DateOnly HIREDATE { get; set; }
        private int? SAL { get; set; }
        private int? COMM { get; set; }
        private int dept_id { get; set; }
    }
}
