﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ_CrashCourse
{
    internal class Department
    {
        public Department(int dEPTNO, string rESEARCH, string lOC)
        {
            DEPTNO = dEPTNO;
            RESEARCH = rESEARCH;
            LOC = lOC;
        }

        private int DEPTNO { get; set; }
        private string RESEARCH { get; set; }
        private string LOC { get; set; }
    }
}
