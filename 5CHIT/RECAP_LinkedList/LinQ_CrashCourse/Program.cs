﻿using LinQ_CrashCourse;
using MySqlConnector;
using System.Linq;

MySqlConnection conn = new MySqlConnection("host=localhost;user=root;database=scottnew");
conn.Open();
MySqlCommand cmd = conn.CreateCommand();
cmd.CommandText = "SELECT * FROM emps";
MySqlDataReader reader = cmd.ExecuteReader();
List<Employee> employees = new List<Employee>();
List<Department> departments = new List<Department>();

while (reader.Read())
{
    int parent_id = 0;
    if (!reader.IsDBNull(3))
    {
        parent_id = reader.GetInt32(3);
    }
    int sal = 0;
    if (!reader.IsDBNull(5))
    {
        sal = reader.GetInt32(5);
    }

    int comm = 0;
    if (!reader.IsDBNull(6))
    {
        comm = reader.GetInt32(6);
    }

    int dept_id = 0;
    if (!reader.IsDBNull(7))
    {
        comm = reader.GetInt32(7);
    }

    Employee emp = new Employee(
        reader.GetInt32(0),
    reader.GetString(1),
    reader.GetString(2),
    parent_id,
    reader.GetDateOnly(4),
    sal,
    comm,
    dept_id
    );
        employees.Add(emp);
    

}
foreach (var item in employees)
{
    Console.WriteLine(item.ToString()) ;
}
 conn.Close();
conn.Open();
cmd = conn.CreateCommand();
cmd.CommandText = "SELECT * FROM depts";
reader = cmd.ExecuteReader();
while (reader.Read())
{
    int deptno = 0;
    if (!reader.IsDBNull(0))
    {
        deptno = reader.GetInt32(0);
    }
    string research = "";
    if (!reader.IsDBNull(1))
    {
        research = reader.GetString(1);
    }

    string loc = "";
    if (!reader.IsDBNull(2))
    {
        loc = reader.GetString(2);
    }

    Department emp = new Department(
    deptno,
    research,
    loc
    );
   departments.Add(emp);


}
foreach (var item in departments)
{
    Console.WriteLine(item.ToString());
}