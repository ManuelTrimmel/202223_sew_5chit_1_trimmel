using System;
using System.Drawing;
using System.Security.Cryptography;

namespace Equalizer
{
    public partial class Form1 : Form
    {
        Random random;

        public int[] values;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();

        }

        public void drawPanels()
        {
            for (int i = 0; i < values.Length; i++)
            {
                //store a random color in a variable
                Color color = Color.SeaGreen;

                for (int j = 0; j < values[i]; j++)
                {
                    Panel panel = new Panel();
                    panel.BackColor = Color.Black;
                    panel.Size = new Size(55, 35);
                    panel.Location = new Point(
                        20 + i * 60,
                        this.ClientSize.Height / 2 - (20 + j * 40) - (panel.Size.Height / 2)
                    );
                    if (panel.Location.Y < ClientSize.Height / 8)
                    {
                        panel.BackColor = Color.Red;
                    }
                    else if (panel.Location.Y < ClientSize.Height / 6)
                    {
                       panel.BackColor = Color.Yellow;
                    }
                    else
                    {
                        panel.BackColor = color;
                    }
          

                    this.Controls.Add(panel);
                }
                for (int j = 0; j < -values[i]; j++)
                {
                    Panel panel = new Panel();
                    panel.BackColor = Color.Black;
                    panel.Size = new Size(55, 35);
                    panel.Location = new Point(
                        20 + i * 60,
                        this.ClientSize.Height / 2 - (20 - j * 40) - (panel.Size.Height / 2)
                    );
                    if (panel.Location.Y >= ClientSize.Height - (ClientSize.Height/8) -40 )
                    {
                        panel.BackColor = Color.Red;
                    }
                    else if (panel.Location.Y >= ClientSize.Height - (ClientSize.Height / 6) - 40)
                    {
                        panel.BackColor = Color.Yellow;
                    }
                    else
                    {
                        panel.BackColor = color;
                    }
                    

                    this.Controls.Add(panel);
                }


            }
        }
        public void GenerateNewValues()
        {
            values = new int[this.ClientSize.Width / 60];
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = random.Next(-8, 8);
            }
            this.Controls.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            random = new Random();
            GenerateNewValues();
            drawPanels();
        }
    }
}