﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesUndSo2
{
    internal class Person
    {
        public delegate void Celebrate(string s);
        public event Celebrate Erwachsen;

        public EventHandler Pension;

        public int Age { get; set; }

        public void Geburtstagfeiern()
        {
            Age++;
            if (Age ==18)
            {
                Erwachsen("Juhuu, ich bin " + Age);
            }
            if (Age==70)
            {
                Pension(this, new MyEventArgs { msg = Age.ToString() });
            }
        }
    }

    class MyEventArgs : EventArgs
    {
        public string msg;
    }
}
