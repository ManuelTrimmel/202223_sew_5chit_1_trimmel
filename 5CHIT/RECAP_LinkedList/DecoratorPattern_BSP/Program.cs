<<<<<<< HEAD
﻿    public class Program
    {
        public static void Main(string[] args)
        {
            // Create ConcreteComponent and two Decorators
            ConcreteComponent c = new ConcreteComponent();
            ConcreteDecoratorA d1 = new ConcreteDecoratorA();
            ConcreteDecoratorB d2 = new ConcreteDecoratorB();
            // Link decorators
            d1.SetComponent(c);
            d2.SetComponent(d1);
            d2.Operation();
            // Wait for user
            Console.ReadKey();
        }
    }
    /// <summary>
    /// The 'Component' abstract class
    /// </summary>
    public abstract class Skifahrer
    {
        public abstract void Operation();
    }
    /// <summary>
    /// The 'ConcreteComponent' class
    /// </summary>
    public class ConcreteComponent : Skifahrer
    {
        public override void Operation()
        {
            Console.WriteLine("ConcreteComponent.Operation()");
        }
    }
    /// <summary>
    /// The 'Decorator' abstract class
    /// </summary>
    public abstract class Decorator : Compone
    {
        protected Component component;
        public void SetComponent(Component component)
        {
            this.component = component;
        }
=======
﻿
using System.Collections.Concurrent;

public class Program
{
        public static void Main(string[] args)
        {
            // Create ConcreteComponent and two Decorators

            ConcreteSkifahrer c = new ConcreteSkifahrer { Name = "Hermann Klein"};
            Olympiasieger d1 = new Olympiasieger();
            Weltcupsieger d2 = new Weltcupsieger();
            Weltcupsieger WCD= new Weltcupsieger { Jahr = 1988};
        Weltcupsieger WCD1 = new Weltcupsieger { Jahr = 2019 };

        d1.SetComponent(c);
            WCD.SetComponent(d1);
        WCD1.SetComponent(WCD);
            WCD1.Operation();


        Console.WriteLine("\n.------------.");
        ConcreteSkifahrer Paul = new ConcreteSkifahrer { Name = "Paul Scheidl" };
            d2.SetComponent(Paul);
            d2.Operation();
        // Link decorators
        Console.WriteLine("\n.------------.");
        ConcreteSkifahrer Lorenz = new ConcreteSkifahrer { Name = "Marcel Hirscher" };
        d1.SetComponent(Lorenz);
        d1.Operation();



            // Wait for user

            Console.ReadKey();
        }
    }

    public abstract class ASkifahrer
    {
        public abstract void Operation();
    }


    public class ConcreteSkifahrer : ASkifahrer
    {
        public string Name { get; init; }
        public override void Operation()
        {
            Console.Write($"Ich bin {Name} ");
        }
    }

    public abstract class ADecorator : ASkifahrer
    {
        protected ASkifahrer component;

        public void SetComponent(ASkifahrer component)
        {
            this.component = component;
        }

>>>>>>> 6a886fb802c4ad35b91f973cf210464cb740f9e3
        public override void Operation()
        {
            if (component != null)
            {
                component.Operation();
            }
        }
<<<<<<< HEAD
    }
    /// <summary>
    /// The 'ConcreteDecoratorA' class
    /// </summary>
    public class ConcreteDecoratorA : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.WriteLine("ConcreteDecoratorA.Operation()");
        }
    }
    /// <summary>
    /// The 'ConcreteDecoratorB' class
    /// </summary>
    public class ConcreteDecoratorB : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            AddedBehavior();
            Console.WriteLine("ConcreteDecoratorB.Operation()");
        }
        void AddedBehavior()
        {
        }
=======
>>>>>>> 6a886fb802c4ad35b91f973cf210464cb740f9e3
    }

    /// <summary>
    /// The 'ConcreteDecoratorA' class
    /// </summary>

    public class Olympiasieger : ADecorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.Write("Olympiasieger");
        }
    }

    /// <summary>
    /// The 'ConcreteDecoratorB' class
    /// </summary>

    public class Weltcupsieger : ADecorator
    {
    public int Jahr { get; init; } 
        public override void Operation()
        {
            base.Operation();
            AddedBehavior();
            Console.Write($"Weltcupsieger im Jahr: {Jahr} ");
        }

        void AddedBehavior()
        {
        Console.Write(", Pokal: ");
        }
    }

