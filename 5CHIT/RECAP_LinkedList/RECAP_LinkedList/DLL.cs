﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RECAP_LinkedList
{
    public class Element<T>
    {
        public T? Value;
        public Element<T> next;
        public Element<T> prev;
        public Element(T el) 
        { Value = el; }
    }
    public class DDL<T> where T : IComparable
    {
        public Element<T> Tail { get; set; }
        public Element<T> Head { get; set; }
        public virtual void Insert(T el)
        {
   
            Element<T> new_Node = new Element<T>(el);

            new_Node.next = Head;
            new_Node.prev = null;

            if (Head != null)
                Head.prev = new_Node;

            Head = new_Node;
        }
        //public Element<T> Find(T value)
        //{
        //    return Element<T> leer;
        //}
        public void Insert_After(Element<T> after_which, T el)
        {

        }
        public void Append(T el)
        {
            Element<T> new_Node = new Element<T>(el);

            Element<T> last = Head; 
            new_Node.next = null;

            if (Head == null)
            {
                new_Node.prev = null;
                Head = new_Node;
                return;
            }

            while (last.next != null)
                last = last.next;

            last.next = new_Node;
            new_Node.prev = last;

        }
        public void reverse()
        {
            Element<T> temp = null;
            Element<T> current = Head;

            /* swap next and prev for all nodes of
            doubly linked list */
            while (current != null)
            {
                temp = current.prev;
                current.prev = current.next;
                current.next = temp;
                current = current.prev;
            }

            if (temp != null)
            {
                Head = temp.prev;
            }
        }
        public void printlist(Element<T> node)
        {

            while (node != null)
            {
                Console.Write(node.Value + " ");
                node = node.next;
            }

            Console.WriteLine();
        }
    }
}
