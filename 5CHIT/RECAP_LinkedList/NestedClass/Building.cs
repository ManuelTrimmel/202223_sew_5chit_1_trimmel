﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedClass
{
    class Building
    {
        Floor[] floors;
        public Building(int e)
        {
            floors = new Floor[e];
            for (int i = 0; i < floors.Length; i++)
            {
                floors[i] = new Floor();
            }
        
        }
        class Floor //not usable
        {

        }
        public class Room
        {

        }
    }
}
