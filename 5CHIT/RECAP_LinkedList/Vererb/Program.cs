﻿
using System.IO.Pipes;
using System.Reflection.Metadata.Ecma335;

List<Animal> animals = new List<Animal>();
animals.Add(new Dog());
animals.Add(new Cat());
animals.Add(new Dog());
animals.Add(new Dog());
animals.Add(new Bird());
animals.Add(new Animal());

List<Bird> lb = new List<Bird>();
lb.Add(new Bird { WingSpan = 44 });
lb.Add(new Bird { WingSpan = 34 });
lb.Add(new Bird { WingSpan = 24 });
lb.Sort();


List<IFlyable> list = new List<IFlyable>();
list.AddRange(lb);
list.Add(new Maturant());


foreach (var item in animals)
{
    if (item is IFlyable) Console.WriteLine("i kaun fliegen");

    item.Fressen();
    if (item is Cat)
    {
        (item as Cat).Schnurren();
    }
    if (item is Dog)
    {
        (item as Dog).Bark();
    }
    Console.WriteLine("---------------------");
}

//foreach (IFlyable item in animals)
//{
//    try
//    {
//        Console.WriteLine(item);

//    }
//    catch (Exception)
//    {

//        throw;
//    }

//}
abstract public class Creature
{
    public abstract void Fressen3();

}

public class Animal: Creature{
    public virtual void Fressen()
    {
        Console.WriteLine("num num");
    }
    public void Fressen2()
    {
        Console.WriteLine("num num");
    }
    public override void Fressen3()
    {
        Console.WriteLine("OH NOM NOM NOM");
    }
}

public class Cat: Animal
{
    public void Schnurren() => Console.WriteLine("hrrrrrrrrrrrrrrrr");
    public void Fressen()
    {
        Console.WriteLine("Mir schmeckt KitteKat");
    }
}

public class Dog: Animal
{
    public void Bark() => Console.WriteLine("woof wood");
    public override void Fressen()
    {
        Console.WriteLine("Mir schmecken Knochen");
    }

}

interface IFlyable
{
    public void Fly();
}

public class Bird: Animal, IFlyable, IComparable
{
    public int WingSpan;
    public int CompareTo(object? obj)
    {
        return this.WingSpan.CompareTo((obj as Bird).WingSpan);
    }
    public void Fly() => Console.WriteLine("I FLY");


}

public class Maturant : IFlyable
{
    public void Fly() => Console.WriteLine("Durchgeflogen");
}