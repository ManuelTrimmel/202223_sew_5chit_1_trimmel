﻿

tuwas(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);

int[] data = { 1, 2, 3, 4, 5, 6 };
Tuwas2(data);

tuwas3("Bimbo", 1, 2, 3, 4, 5, 6, 7, 8);


PlaySong();

static void PlaySong()
{
    Beep(1320, 500); Beep(990, 250); Beep(1056, 250); Beep(1188, 250); Beep(1320, 125); Beep(1188, 125); Beep(1056, 250); Beep(990, 250); Beep(880, 500); Beep(880, 250); Beep(1056, 250); Beep(1320, 500); Beep(1188, 250); Beep(1056, 250); Beep(990, 750); Beep(1056, 250); Beep(1188, 500); Beep(1320, 500); Beep(1056, 500); Beep(880, 500); Beep(880, 500); System.Threading.Thread.Sleep(250); Beep(1188, 500); Beep(1408, 250); Beep(1760, 500); Beep(1584, 250); Beep(1408, 250); Beep(1320, 750); Beep(1056, 250); Beep(1320, 500); Beep(1188, 250); Beep(1056, 250); Beep(990, 500); Beep(990, 250); Beep(1056, 250); Beep(1188, 500); Beep(1320, 500); Beep(1056, 500); Beep(880, 500); Beep(880, 500); System.Threading.Thread.Sleep(500); PlaySong();
}


static void Beep(int f, int d)
{
	Console.Beep(f, d);
}

/* Eine Musiknote, die 10 Töne vom Kammerton A (440 Hz)
 * entfernt ist */
Note note = new Note(10);
/* Implizite Konvertierung nach Double (in Hertz) */
double hertz = note; // nächste Frequenz ist 494 Hz Note n = (Note)hertz; // = Ton H = 2 Töne Abstand

static void Tuwas2(int[] i)
{
	foreach (var item in i)
	{
		Console.WriteLine(item);
	}
}



static void tuwas(params int[] i)
{
	foreach (var item in i)
	{
		Console.WriteLine(item);
	}
}

static void tuwas3(string x, params int[] i)
{
	Console.WriteLine(x);
    foreach (var item in i)
    {
        Console.WriteLine(item);
    }
}

Wurscht w = new Wurscht();
w.Machwas(1);
w.Machwas(1, 2);
w.Machwas(1, 2, 3);

w.MachwasBetter(1, 2, 3);

class Wurscht
{
	public void Machwas(int i) { }

	public void Machwas(int i, int j) { }

	public void Machwas(int i, int j, int k) { }

	public void MachwasBetter(int i, int j =0, int k = 0) { } //kann auf 3 Arten aufgerufen werden

}

public class Note
{
    /* Die Eigenschaft Position speichert den Abstand
    * zwischen dem Kammerton A und dieser Note */
    public int Position;
    public Note(int position)
    {
        this.Position = position;
    }
    /* Operator zum impliziten Konvertieren einer
     * Note nach double */
    public static implicit operator double(Note n)
    {
        return 440 * Math.Pow(2,
        (double)n.Position / 12);
    }
    /* Operator zum expliziten Konvertieren eines double-Werts in
     * eine Note. Ein double-Wert kann nur annähernd in eine Note
     * konvertiert werden. */
    public static explicit operator Note(double x)
    {
        return new Note((int)(0.5 + 12 *
        (Math.Log(x / 440) / Math.Log(2))));
    }
}