﻿namespace AmpelThreads
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelred = new System.Windows.Forms.Panel();
            this.panelyellow = new System.Windows.Forms.Panel();
            this.panelgreen = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelred
            // 
            this.panelred.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelred.Location = new System.Drawing.Point(313, 60);
            this.panelred.Name = "panelred";
            this.panelred.Size = new System.Drawing.Size(100, 77);
            this.panelred.TabIndex = 3;
            this.panelred.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panelyellow
            // 
            this.panelyellow.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelyellow.Location = new System.Drawing.Point(313, 143);
            this.panelyellow.Name = "panelyellow";
            this.panelyellow.Size = new System.Drawing.Size(100, 77);
            this.panelyellow.TabIndex = 4;
            // 
            // panelgreen
            // 
            this.panelgreen.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelgreen.Location = new System.Drawing.Point(313, 226);
            this.panelgreen.Name = "panelgreen";
            this.panelgreen.Size = new System.Drawing.Size(100, 77);
            this.panelgreen.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panelgreen);
            this.Controls.Add(this.panelyellow);
            this.Controls.Add(this.panelred);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panelred;
        private Panel panelyellow;
        private Panel panelgreen;
    }
}