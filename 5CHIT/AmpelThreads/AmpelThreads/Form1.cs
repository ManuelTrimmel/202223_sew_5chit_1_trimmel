namespace AmpelThreads
{
    public partial class Form1 : Form
    {
        private Semaphore green = new Semaphore(1,1);
        private Semaphore red = new Semaphore(0,1);
        private Semaphore yellow = new Semaphore(0, 1);
        private Semaphore greenblink = new Semaphore(0, 1);
        private Semaphore yellowred = new Semaphore(0, 1);
        List<Thread> threads = new List<Thread>();
        Boolean fertig = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread greenT = new Thread(GreenThread);
            Thread redT = new Thread(RedThread);
            Thread yellowT = new Thread(YellowThread);
            Thread greenblinkT = new Thread(GreenBlinkThread);
            Thread yellowredT = new Thread(YellowRedThread);

            threads.Add(greenT);
            threads.Add(redT);
            threads.Add(yellowT);
            threads.Add(greenblinkT);
            threads.Add(yellowredT);

            foreach (var item in threads)
            {
                item.Start();
            }
        }


        public void GreenThread()
        {
            while (true)
            {
                green.WaitOne();
                if (fertig) return;

                panelgreen.Invoke(() => panelgreen.BackColor = Color.Green);
                Thread.Sleep(2000);
                panelgreen.Invoke(() => panelgreen.BackColor = Color.White);
                greenblink.Release();
                    
            }

        }

        public void YellowThread()
        {
            while(true)
            {
                yellow.WaitOne();
                if (fertig) return;

                panelyellow.Invoke(() => panelyellow.BackColor = Color.Yellow);
                    Thread.Sleep(2000);
                panelyellow.Invoke(() => panelyellow.BackColor = Color.White);
                red.Release();
                

            }

        }
        public void RedThread()
        {
            while (true)
            {
                red.WaitOne();
                if (fertig) return;
                
                    panelred.Invoke(() => panelred.BackColor = Color.Red);
                    Thread.Sleep(2000);
                    //panelred.BackColor = Color.White;
                    yellowred.Release();
                

            }

        }
        public void GreenBlinkThread()
        {
            while(true)
            {
                greenblink.WaitOne();
                

                for (int i = 0; i < 4; i++)
                    {
                    if (fertig) return;
                    Thread.Sleep(200);
                    panelgreen.Invoke(() => panelgreen.BackColor = Color.Green);
                        Thread.Sleep(200);
                    panelgreen.Invoke(() => panelgreen.BackColor = Color.White);
                    }
                    yellow.Release();
                

            }

        }

        public void YellowRedThread()
        { 
            while (true)
            {
                yellowred.WaitOne();
                if (fertig) return;

                panelyellow.Invoke(() => panelyellow.BackColor = Color.Yellow);
                Thread.Sleep(2000);
                panelred.Invoke(() => panelred.BackColor = Color.White);
                panelyellow.Invoke(() => panelyellow.BackColor = Color.White);
                green.Release();
                

            }

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            fertig = true;

            greenblink.Release();
            green.Release();
            red.Release();
            yellow.Release();
            yellowred.Release();

            foreach (var item in threads)
            {
                item.Join();
            }
           


        }
    }
}