﻿

using System.Diagnostics.Contracts;

Vektor vek1 = new Vektor(1, 2);
Vektor vek2 = new Vektor(2, 2);
Vektor vek3;
vek3 = vek1 + vek2;
Console.WriteLine(vek1);
Console.WriteLine(vek2);
Console.WriteLine(vek3);

Vektor vek4 = new Vektor(1, 1);

Vektor vek5 = vek3 - vek4;
Console.WriteLine(vek5);
Console.WriteLine("------------------------");

VektorenSammlung vs = new VektorenSammlung();
vs[0] = vek1;
vs[1] = vek2;
Console.WriteLine(vs[1]);

public class Vektor
{
    int a, b;
    public Vektor(int a, int b)
    {
        this.a = a;
        this.b = b;
    }

    public override string ToString()
    {
        return $"A: {a} & B: {b}"; 
    }

    public static Vektor operator +(Vektor v1, Vektor v2)
    {
        Vektor returnVek = new Vektor(v1.a + v2.a, v1.b + v2.b);
        return returnVek;
    }
    public static Vektor operator -(Vektor v1, Vektor v2) => new Vektor(v1.a - v2.a, v1.b - v2.b);

}

public class VektorenSammlung
{
    private List<Vektor> vekList = new List<Vektor>();
    public Vektor this[int index]
    {
        get { return vekList[index]; }
        set { if (vekList.Count > index) {
                vekList[index] = value;
            }
        else
            {
                vekList.Add(value);
            }
        ; }
    }
}


