﻿using System.Globalization;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

Hacho h = new Hacho();
h.machoman();

Console.WriteLine("----------------");

HachomitInter hmit = new HachomitInter();
hmit.name = "Mehrweg";
hmit.machoman();
Console.WriteLine("------------------");
A einser = new A("AAAEEE");
B zweier = new B();
Console.WriteLine(einser.h);


Wachhund w = new Wachhund();
Hund hund = new Hund();
if (w is Hund)
{
    Console.WriteLine("apaneseCalendar er is einser nhund");
    
}

public abstract class Mehrmeg{

    public abstract void machoman();

    public int integer = 1;

    public string stringer = "aaa";
}

public class Hacho : Mehrmeg
{
    public override void machoman()
    {
        Console.WriteLine("machoman überschrieben");
        Console.WriteLine(base.integer);
        Console.WriteLine(base.stringer);
    }
}

public interface IMehrweg
{
    public abstract void machoman();
    public string name { get; set; }

    public void machoman2();

}

public class HachomitInter : IMehrweg, IComparable<HachomitInter>
{
    private string _name;
    public string name
    {
        get => _name;
        set => _name = value;
    }

    public int CompareTo(HachomitInter? other)
    {
        throw new NotImplementedException();
    }

    public void machoman()
    {
        //name = "challo";

        Console.WriteLine(name);
    }

    public void machoman2()
    {
        throw new NotImplementedException();
    }
}

public class A
{
    public readonly String h;
    public A()
    {

    }
    public A(String h)
    {
        this.h = h;
        Console.WriteLine("Hallo bin a do");
    }
}

public class B : A
{ 
    public B(int h)
    {

    }
    public B() : base("")
    {

    }
}

public class Hund
{
    protected int age;
    protected void sitz()
    {
        Console.WriteLine("mache Sitz Hund");
    }
    public Hund()
    {
        Console.WriteLine("ich bin ein hund");
    }
}

public class Wachhund : Hund
{
    public void sitzen()
    {
        sitz();
        age = 69;
    }
}