﻿// See https://aka.ms/new-console-template for more information
using System.Security.Principal;

Console.WriteLine("Hello, World!");
Mann m = new Mann("Alex", 22);

Frau f = m;


Frau f2 = new Frau("Alexandra", 24);
Mann m2= f2;

Console.WriteLine(m2.isMale.ToString());
Console.WriteLine(f.isMale.ToString());

public abstract class Mensch
{
    public string name;
    public int age;
    public bool isMale;
}

public class Mann : Mensch
{
    public Mann(string name, int age)
    {
        this.age = age;
        this.name = name;
        this.isMale = true;
    }
    public static implicit operator Mann(Frau frau)
    {
        Mann mann = new Mann(frau.name, frau.age);
        return mann;
    }
}
public class Frau : Mensch
{
    public Frau(string name, int age)
    {
        this.age = age;
        this.name = name;
        this.isMale=false;
    }
    public static implicit operator Frau(Mann mann)
    {
        Frau frau = new Frau(mann.name, mann.age);
        return frau;
    }


}