﻿using System.Net.Sockets;
using System.Net;
using System.Text;
using System;

TcpListener lsn = new TcpListener(IPAddress.Any, 2022);
lsn.Start();

for (int i = 0; i < 5; i++)
{
    new Thread(() => Service()).Start();
}

void Service()
{
    while (true)
    {
        Socket s = lsn.AcceptSocket(); // Checks for new messages

        Console.WriteLine(s.LocalEndPoint);
        Console.WriteLine(s.RemoteEndPoint);

        string request;
        NetworkStream ns = new NetworkStream(s);
        StreamReader sr = new StreamReader(ns);
        StreamWriter sw = new StreamWriter(ns);
        sw.AutoFlush = true;

        request = sr.ReadLine();
        Console.WriteLine(request);

        string filename = request != null ? request.Split(" ")[1].Substring(1) : "";
        string content;
        string message;

        if (File.Exists(filename))
        {
            sr = new StreamReader(filename);
            content = sr.ReadToEnd();
            message = CreateHttpMessage("200 OK", content);
        }
        else
        {
            content = "<h1>Invalid Filename</h1>";
            message = CreateHttpMessage("404 Not Found", content);

            //byte[] message = Encoding.ASCII.GetBytes(httpHeader + content);
            //s.Send(message, message.Length, 0);
        }
        sw.WriteLine(message);
    }
}

string CreateHeader(string httpVersion, string httpStatusCode, int contentLength)
{
    return $"{httpVersion} {httpStatusCode}\r\n" +
            "Server: SEW\r\n" +
            "Content-Type: text/html\r\n" +
           $"Content-Length: {contentLength}\r\n\r\n";
}

string CreateHttpMessage(string httpStatusCode, string content)
{
    string message = CreateHeader("HTTP/1.1", httpStatusCode, content.Length) + content;
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(message);
    Console.ForegroundColor = ConsoleColor.White;
    return message;
}