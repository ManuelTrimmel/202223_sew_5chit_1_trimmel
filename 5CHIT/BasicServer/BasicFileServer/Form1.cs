using System.Net.Sockets;
using System.Net;

namespace BasicFileServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            int Limit = 5;

            TcpListener lsn;

            lsn = new TcpListener(IPAddress.Any, 2022);
            lsn.Start();

            for (int i = 0; i < Limit; i++)
            {
                new Thread(new ThreadStart(Service)).Start();
            }

            void Service()
            {
                Socket s = lsn.AcceptSocket();
                while (true)
                {
                    //Console.WriteLine(s.RemoteEndPoint.ToString());
                    //Console.WriteLine(s.LocalEndPoint.ToString());

                    NetworkStream sStream = new NetworkStream(s);

                    StreamWriter sw = new StreamWriter(sStream);
                    StreamReader sr = new StreamReader(sStream);
                    string request = sr.ReadLine();
                    //Console.WriteLine("read request" + request);
                    sw.WriteLine(request.ToUpper());
                    sw.Flush();
                    //Console.WriteLine("Ende");
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}