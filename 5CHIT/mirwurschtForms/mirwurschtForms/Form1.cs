using System.Windows.Forms.VisualStyles;
using System.Xml;
using System.Xml.Linq;

namespace mirwurschtForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //XmlDocument xmldoc = new XmlDocument();
            //XmlNode xmlnode;
            //FileStream fs = new FileStream("oasch.xml", FileMode.Open, FileAccess.Read);
            //xmldoc.Load(fs);
            //xmlnode = xmldoc.ChildNodes[1];
            //treeView1.Nodes.Clear();
            //treeView1.Nodes.Add(new TreeNode(xmldoc.DocumentElement.Name));
            //TreeNode tNode;
            //tNode = treeView1.Nodes[0];
            //AddNode(xmlnode, tNode);



            //TreeNode t = new TreeNode();
            //t.Nodes.Add("NodeItem");
            //t.Nodes.Add("NdoeItem2");
            //TreeNode sub = t.Nodes.Add("SubNode");
            //sub.Nodes.Add("subnideItem");

            //treeView1.Nodes.Add(t);
            funktionhoid();
        }
        private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            XmlNode xNode;
            TreeNode tNode;
            XmlNodeList nodeList;
            int i = 0;
            if (inXmlNode.HasChildNodes)
            {
                nodeList = inXmlNode.ChildNodes;
                for (i = 0; i <= nodeList.Count - 1; i++)
                {
                    xNode = inXmlNode.ChildNodes[i];
                    inTreeNode.Nodes.Add(new TreeNode(xNode.Name));
                    tNode = inTreeNode.Nodes[i];
                    AddNode(xNode, tNode);
                }
            }
            else
            {
                inTreeNode.Text = inXmlNode.InnerText.ToString();
            }
        }
        public void funktionhoid()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            XDocument doc = XDocument.Load(ofd.FileName);

            treeView1.Nodes.Clear();
            FillTree(treeView1.Nodes.Add("Content"), doc.Root);
        }

        public void FillTree(TreeNode current, XElement xe)
        {
            XAttribute xa = xe.Attribute("Name");
            TreeNode tn = current.Nodes.Add(xa.Value);
            foreach (var item in xe.Elements())
            {
                FillTree(tn, item);
            }
        }
    }
    
}