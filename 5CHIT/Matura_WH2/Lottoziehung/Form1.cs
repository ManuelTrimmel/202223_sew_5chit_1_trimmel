using System;
using System.Drawing.Printing;

namespace Lottoziehung
{
    public partial class Form1 : Form
    {
        List<Button> buttons;
        int lottoanzahl;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Random random = new Random();
            buttons  = new List<Button>();
            lottoanzahl = 0;
            for (int i = 1; i <= 45; i++)
            {
                int buttonlocheight = random.Next(20, groupBox1.Size.Height- 20);
                int buttonlocwidth =  random.Next(20, groupBox1.Size.Width -20);
                buttons.Add(new Button { Name = "_" + i.ToString(), Text = i.ToString() , Location = new Point(buttonlocwidth, buttonlocheight), Size = new Size(40,40) });
            }
            foreach (Button button in buttons)
            {
                groupBox1.Controls.Add(button);
                button.Click += Lottozahlen_Click;
            }
            //ziehebutton.Enabled = false;
            newbutton.Enabled = false;
        }

        private void newbutton_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            groupBox1.Controls.Clear();
            Form1_Load(sender, e);
        }

        private void mischebutton_Click(object sender, EventArgs e)
        {
            mischen();
        }

        private void ziehebutton_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            //int anzahlRichtig = 0;
            int rand = random.Next(1, 45);
            
            foreach (Button button in flowLayoutPanel1.Controls) { 
                if (button.Text == rand.ToString())
                {
                    rand = random.Next(1, 45);
                }
            }
            if (lottoanzahl<6)
            {
                groupBox1.Controls.Remove(buttons[rand]);
                flowLayoutPanel1.Controls.Add(buttons[rand]);
                newbutton.Enabled = false;
                lottoanzahl++;
            }
            
            
            if (lottoanzahl >= 5)
            {
                newbutton.Enabled = true;
            }
            

            //MessageBox.Show("So viele waren Richtig: " + anzahlRichtig.ToString());
            //newbutton.Enabled = true;
            
        }

        private void Lottozahlen_Click(object? sender, EventArgs e)
        {
            
            //if (lottoanzahl != 6)
            //{
            //    groupBox1.Controls.Remove((Button)sender);
            //    flowLayoutPanel1.Controls.Add((Button)sender);
            //    //ziehebutton.Enabled = false;
            //}
            //if (lottoanzahl == 5)
            //{
            //    ziehebutton.Enabled = true;
            //}

            //lottoanzahl++;

        }

        private void mischen()
        {
            Random random = new Random();
            foreach (Button button in buttons)
            {
                int buttonlocheight = random.Next(20, groupBox1.Size.Height - 20);
                int buttonlocwidth = random.Next(20, groupBox1.Size.Width - 20);
                button.Location = new Point(buttonlocwidth,buttonlocheight);
            }
        }
    }
}