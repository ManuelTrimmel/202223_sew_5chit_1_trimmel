﻿namespace Lottoziehung
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            newbutton = new Button();
            mischebutton = new Button();
            ziehebutton = new Button();
            flowLayoutPanel1 = new FlowLayoutPanel();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.BackColor = Color.White;
            groupBox1.BackgroundImageLayout = ImageLayout.None;
            groupBox1.FlatStyle = FlatStyle.Flat;
            groupBox1.Location = new Point(32, 30);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(420, 333);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            // 
            // newbutton
            // 
            newbutton.Location = new Point(660, 30);
            newbutton.Name = "newbutton";
            newbutton.Size = new Size(75, 23);
            newbutton.TabIndex = 1;
            newbutton.Text = "new";
            newbutton.UseVisualStyleBackColor = true;
            newbutton.Click += newbutton_Click;
            // 
            // mischebutton
            // 
            mischebutton.Location = new Point(660, 75);
            mischebutton.Name = "mischebutton";
            mischebutton.Size = new Size(75, 23);
            mischebutton.TabIndex = 2;
            mischebutton.Text = "mische";
            mischebutton.UseVisualStyleBackColor = true;
            mischebutton.Click += mischebutton_Click;
            // 
            // ziehebutton
            // 
            ziehebutton.Location = new Point(660, 125);
            ziehebutton.Name = "ziehebutton";
            ziehebutton.Size = new Size(75, 23);
            ziehebutton.TabIndex = 3;
            ziehebutton.Text = "ziehe";
            ziehebutton.UseVisualStyleBackColor = true;
            ziehebutton.Click += ziehebutton_Click;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.BackColor = Color.LightGreen;
            flowLayoutPanel1.Location = new Point(570, 175);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(275, 188);
            flowLayoutPanel1.TabIndex = 4;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(904, 450);
            Controls.Add(flowLayoutPanel1);
            Controls.Add(ziehebutton);
            Controls.Add(mischebutton);
            Controls.Add(newbutton);
            Controls.Add(groupBox1);
            Name = "Form1";
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private Button newbutton;
        private Button mischebutton;
        private Button ziehebutton;
        private FlowLayoutPanel flowLayoutPanel1;
    }
}