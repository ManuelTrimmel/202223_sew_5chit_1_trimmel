﻿using System;
using System.Collections;

LinkedList lili = new LinkedList();
lili.InsertFront(new Node { Value = "Manuel" });
lili.InsertFront(new Node { Value = "Florian" });
lili.InsertFront(new Node { Value = "Tobias" });


Node anfang = lili.header;

while (anfang != null)
{
    Console.WriteLine(anfang.Value);
    anfang = anfang.NextNode;
}
Console.WriteLine("Mit Indexer");
for (int i = 0; i < 3; i++)
{
    Console.WriteLine(lili[i]);
}
Console.WriteLine("mit GetEnumerator");
foreach (var item in lili)
{
    Console.WriteLine(item);
}

public class Node{
    public Node NextNode { get; set; }
    public string Value { get; set; }

    public override string ToString()
    {
        return Value.ToString();
    }
}

public class LinkedList
{
    public Node header;

    public void InsertFront(Node toInsert)
    {
        toInsert.NextNode = header;
        header = toInsert;  
    }

    public IEnumerator GetEnumerator()
    {
        Node toreturn = header;
        while (toreturn.NextNode != null)
        {
            yield return toreturn;
            toreturn = toreturn.NextNode;
            
        }
        yield return toreturn;
    }

    public Node this[int index]
    {
        get
        {
            Node toreturn = header;
            int count = 0;
            while (toreturn.NextNode != null && count++ < index)
            {
                toreturn = toreturn.NextNode;
            }
            return toreturn;
        }
    }

}