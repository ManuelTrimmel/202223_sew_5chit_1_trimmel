﻿//IEnumerable

using System.Collections;

LinkedList _5chit = new LinkedList();
_5chit.InsertFront(new Node { Value = "Trimmel" });
_5chit.InsertFront(new Node { Value = "Schlosser" });
_5chit.InsertFront(new Node { Value = "Scheidl" });

Node anfang = _5chit.Head;
Console.WriteLine("mit einer normalen While Schleife");
while (anfang != null)
{
    Console.WriteLine(anfang.Value);
    anfang = anfang.Next;
}

Console.WriteLine("Mit einer For Schleife (Indexer)");
for (int i = 0; i < 3; i++)
{
    Console.WriteLine(_5chit[i]);
}
Console.WriteLine("Mit einer For-Each Schleife (IEnumerable => IEnumerator)");
foreach (Node item in _5chit)
{
    Console.WriteLine(item.Value);
}



public class Node
{
    public Node Next { get; set; }

    public string Value { get; set; }
    public override string ToString()
    {
        return Value;
    }
}


public class LinkedList //:IEnumerable => braucht man irgendwie nicht 
{
    public Node Head;
    public void InsertFront(Node toadd)
    {
        toadd.Next = Head;
        Head = toadd;
    }

    public Node this[int i]
    {
        get
        {
            Node toReturn = Head;
            int j = 0;
            while (toReturn.Next != null && j++ < i)
                toReturn = toReturn.Next;
            return toReturn;
        }
    }

    public IEnumerator GetEnumerator()
    {
        Node toReturn = Head;
        while (toReturn.Next != null)
        {
            yield return toReturn;
            toReturn = toReturn.Next;
        }
            
        yield return toReturn;
        //Yiels merkt sich im Code beim returnen wo es war.
    }

}
