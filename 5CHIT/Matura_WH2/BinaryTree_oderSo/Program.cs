﻿BinTree bamoida = new BinTree();
bamoida.Insert(new Node { Value = "M" });
bamoida.Insert(new Node { Value = "A" });
bamoida.Insert(new Node { Value = "N" });
bamoida.Insert(new Node { Value = "U" });
bamoida.Insert(new Node { Value = "E" });
bamoida.Insert(new Node { Value = "L" });

bamoida.InOrderOut();


public class Node
{
    public string Value { get; set; }
    public Node Left;
    public Node Right;

}

public class BinTree
{
    public Node Root;
    public void Insert(Node toInsert)
    {
        if (Root == null)
        {
            Root = toInsert; // Leerer Baum => erstes Element wird zur Wurzel
        }
        else
        {
            Insert(Root, toInsert); 
        }

    }
    private void Insert(Node where, Node toInsert)
    {
        if (toInsert.Value.CompareTo(where.Value)<0) //Links einfügen
        {
            if (where.Left == null)
                where.Left = toInsert;
            else
                Insert(where.Left, toInsert); //Rekursion wird aufgerufen
        }
        else //sonst Rechts einfügen
        {
            if (where.Right == null)
                where.Right = toInsert;
            else
                Insert(where.Right, toInsert);
        }
    }
    public void InOrderOut() { InOrderOut(Root); }
    public void InOrderOut(Node where)
    {
        if (where == null) return;

        InOrderOut(where.Left);
        Console.WriteLine(where.Value);
        InOrderOut(where.Right);
    }
}
