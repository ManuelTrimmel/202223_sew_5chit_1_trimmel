﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Mars_Mission
{
    static class Sems
    {
        public static int SentinelCNT = 10;
        public static int HarvesterCNT = 20;
        public static int StorageCNT = 4;
        public static Semaphore harvesterready = new Semaphore(0, HarvesterCNT);
        public static Semaphore storage = new Semaphore(StorageCNT, StorageCNT);
        public static Semaphore harvesterneeded = new Semaphore(0, HarvesterCNT);
        public static Semaphore consoleschreiben = new Semaphore(1, 1);
    }
    class Program
    {

        static void Main(string[] args)
        {
            Task[] Sentinels = new Task[Sems.SentinelCNT];
            for (int i = 0; i < Sentinels.Length; i++)
            {
                Sentinels[i] = new Task(new Sentinel().Run);
                Sentinels[i].Start();
               
            }
 

            Task[] Harvesters = new Task[Sems.HarvesterCNT];
            for (int i = 0; i < Harvesters.Length; i++)
            {
                Harvesters[i] = new Task(new Harvester().Run);
                Harvesters[i].Start();
                
            }

            Thread.Sleep(8000);

        }

        public class Sentinel
        {
            public void Run()
            {
                while (true)
                {
                    ScanningSurface();
                    Signal();
                    Sems.harvesterneeded.Release();
                    Sems.harvesterready.WaitOne();
                }
            }

            private void ScanningSurface()
            {
                Sems.consoleschreiben.WaitOne();
                choosefarbe.changefarbe(Thread.CurrentThread.GetHashCode());
                Console.WriteLine($"Scanning Surface {Thread.CurrentThread.GetHashCode()}");
                Sems.consoleschreiben.Release();
                Thread.Sleep(500);
            }

            private void Signal()
            {
                Thread.Sleep(800);
                Sems.consoleschreiben.WaitOne();
                choosefarbe.changefarbe(Thread.CurrentThread.GetHashCode());
                Console.WriteLine($"Found raw material {Thread.CurrentThread.GetHashCode()}");
                Sems.consoleschreiben.Release();
            }
        }

        public class Harvester
        {

            public void Run()
            {
                while (true)
                {
                    Sems.harvesterneeded.WaitOne();
                    Acknowledge();
                    Sems.harvesterready.Release();
                    Harvest();
                    Sems.storage.WaitOne();
                    Store();
                    Sems.storage.Release();
                }

            }



            private void Acknowledge()
            {
                Sems.consoleschreiben.WaitOne();
                choosefarbe.changefarbe(Thread.CurrentThread.GetHashCode());
                Console.WriteLine($"Acknowledgeing signal {Thread.CurrentThread.GetHashCode()}");
                Sems.consoleschreiben.Release();
                Thread.Sleep(100);
            }

            private void Harvest()
            {
                Sems.consoleschreiben.WaitOne();
                choosefarbe.changefarbe(Thread.CurrentThread.GetHashCode());
                Console.WriteLine($"Harvesting Ressources {Thread.CurrentThread.GetHashCode()}");
                Sems.consoleschreiben.Release();
                Thread.Sleep(1000);
            }

            private void Store()
            {
                Sems.consoleschreiben.WaitOne();
                choosefarbe.changefarbe(Thread.CurrentThread.GetHashCode());
                Console.WriteLine($"Storing Ressources {Thread.CurrentThread.GetHashCode()}");
                Sems.consoleschreiben.Release();
                Thread.Sleep(200);
            }
        }
        //Copyright Choosefrage Paul Scheidl
        public static class choosefarbe
        {
            public static void changefarbe(int i)
            {
                int x = i % 256, a = x / 16, b = x % 16;
                if (a == b)
                    a = 17;
                Console.ForegroundColor = (ConsoleColor)(b);
                //Console.BackgroundColor = (ConsoleColor)(a);
            }
        } 
    }
}

   