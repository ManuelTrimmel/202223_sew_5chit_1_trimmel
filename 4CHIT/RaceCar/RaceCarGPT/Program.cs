﻿using System;
using System.Threading;
using System.Threading.Tasks;

class RaceCar
{
    // Semaphore to synchronize the start of the race
    public static Semaphore semaphore = new Semaphore(0, 5);

    public void Run()
    {
        Console.WriteLine("Race car starting engine...");

        // Wait for the signal to start the race
        semaphore.WaitOne();

        Race();
    }

    public void WaitForSignal()
    {
        Console.WriteLine("Race car is ready and waiting for the signal...");
    }

    public void Race()
    {
        Console.WriteLine("Race car is now racing...");
    }

    public void TakingPitStop()
    {
        Console.WriteLine("Race car is taking a pit stop...");
    }

    public void Finished()
    {
        Console.WriteLine("Race car has finished the race!");
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Create an array of tasks to start the race cars
        Task[] tasks = new Task[5];
        for (int i = 0; i < tasks.Length; i++)
        {
            RaceCar car = new RaceCar();
            tasks[i] = Task.Run(() => car.Run());
            car.WaitForSignal();
        }

        // Give the signal to start the race
        Console.WriteLine("Starting the race!");
        RaceCar.semaphore.Release(5);

        // Wait for the tasks to complete
        Task.WaitAll(tasks);
        Console.WriteLine("All race cars have finished the race!");
    }
}