﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RaceCar
{
    static class Sems
    {
        public static Semaphore Wait = new Semaphore(0,5);
        public static Semaphore Pit = new Semaphore(3, 3);
        public static Semaphore finished = new Semaphore(0, 5);
        public static Semaphore start = new Semaphore(0, 5);
    }

    class Program
    {
        static void Main(string[] args)
        {
            Task[] cars = new Task[5];
            for (int i = 0; i < cars.Length; i++)
            {
                cars[i] = new Task(new Car().Run);
                cars[i].Start();
            }
            Race r = new Race();
            r.Run();
        }
    }
    class Car
    {
        public void Run()
        {
            Sems.start.Release();
            WaitForSignal();
            Sems.Wait.WaitOne();
            Race();
            Sems.Pit.WaitOne();
            TakingPitStop();
            Sems.Pit.Release();
            Race();
            

            
            Finished();
            Sems.finished.Release();
        }

        private void WaitForSignal()
        {
            Console.WriteLine($"Waiting for Start {Thread.CurrentThread.GetHashCode()}");
        }
        private void Race()
        {
            Console.WriteLine($"Racing {Thread.CurrentThread.GetHashCode()}");
        }
        private void TakingPitStop()
        {
            Console.WriteLine($"in pit {Thread.CurrentThread.GetHashCode()}");
            Thread.Sleep(25);
        }
        private void Finished()
        {
            Console.WriteLine($"Finished { Thread.CurrentThread.GetHashCode()}");
        }
    }
    class Race
    {
        public void Run()
        {

            for (int i = 0; i < 5; i++)
            {
                Sems.start.WaitOne();
            }
            Thread.Sleep(50);           //let other threads display their output
            Start();
                
            Sems.Wait.Release(5);

            for (int i = 0; i < 5; i++)
            {
                Sems.finished.WaitOne();
            }

           
            End();
        }
        private void Start()
        {
            Console.WriteLine("Starting Race");

        }
        private void End()
        {
            Console.WriteLine("Finished Race");
        }
    }
}
