﻿// See https://aka.ms/new-console-template for more information
using MySqlConnector;
using DependencyInjectionAufgabe3;




MySqlConnection c = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt");
WerkstattVerwaltung WV = new WerkstattVerwaltung(c);

while (true)
{
    Console.Clear();
    ListAll();
    Console.WriteLine("\ne X => Edit id X \nd X => Delete id X \ni => Insert \n");
    string input = Console.ReadLine();

    Int32.TryParse(input?.Substring(2), out int nr);

    switch (input[0])
    {
        case 'e': EditAuto(nr); break;
        case 'd': DeleteAuto(nr); break;
        case 'i': newAuto(); break;
        default: ListAll(); break;
    }

}
void ListAll()
{
    List<auto> werkstatt = WV.GetAll();
    foreach (var item in werkstatt)
    {
        Console.WriteLine($"{item.id}, {item.marke}, {item.modell}");
    }
}
void DeleteAuto(int id)
{
    WV.Delete(new auto { id = id});
}
void EditAuto(int id)
{
    auto a = new auto();
    a.id = id;
    Console.Write("neue Marke: ");
    a.marke = Console.ReadLine();
    Console.Write("neues Modell: ");
    a.modell = Console.ReadLine();

    WV.EditByID(a);
}
void newAuto()
{
    auto a = new auto();
    Console.Write("Marke: ");
    a.marke = Console.ReadLine();
    Console.Write("Modell: ");
    a.modell = Console.ReadLine();
    WV.Insert(a);
}







