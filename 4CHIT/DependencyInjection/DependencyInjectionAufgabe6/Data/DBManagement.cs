﻿
using DependencyInjectionDLL;
using MySqlConnector;
namespace DependencyInjectionAufgabe6.Data
{
    public class DbManagement //Singleton Pattern
    {
        public WerkstattVerwaltung WV { get; private set; }
        static object tmp = new object();   
        public static DbManagement Instance;
        private DbManagement()
        {
            MySqlConnection c = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt");
            WV = new WerkstattVerwaltung(c);
        }
        public static DbManagement getInstance()
        {
            if (Instance != null)
            {
                return Instance;
            }
            lock (tmp)
            {
                Instance = new DbManagement();
            }
            return Instance;
        }
    }
}
