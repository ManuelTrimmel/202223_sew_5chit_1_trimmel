using MySqlConnector;
using DependencyInjectionDLL;


namespace DependencyInjectionAufgabe5
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
       

        }
        auto aktuell;
        WerkstattVerwaltung WV;
        bool check = false;
        private void Form1_Load(object sender, EventArgs e)
        {
            MySqlConnection c = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt");
            WV = new WerkstattVerwaltung(c);
            Refresh();
            //timer1.Start();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = false;
            button2.Enabled = false;
            check = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                WV.Delete(new auto { id = Convert.ToInt32(textBox3.Text), marke = textBox1.Text, modell = textBox2.Text });
                Refresh();
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
            }
        }
        private void Refresh()
        {
            listBox1.Items.Clear();
            listBox1.Items.AddRange(WV.GetAll().ToArray());
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            aktuell = listBox1.SelectedItem as auto;
            textBox3.Text = aktuell.id.ToString();
            textBox1.Text = aktuell.marke;
            textBox2.Text = aktuell.modell;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {

            
            if (check == true)
            {
                WV.Insert(new auto { marke = textBox1.Text, modell = textBox2.Text });
            }
            else
            {
                WV.EditByID(new auto { id = Convert.ToInt32(textBox3.Text), marke = textBox1.Text, modell = textBox2.Text });
            }
            }
            button2.Enabled = true;
            textBox3.Enabled = true;
            textBox1.Clear();
            textBox2.Clear();
            Refresh();
            check = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}