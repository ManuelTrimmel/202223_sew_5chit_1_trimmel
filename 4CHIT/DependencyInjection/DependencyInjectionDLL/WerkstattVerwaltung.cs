﻿using MySqlConnector;
namespace DependencyInjectionDLL
{

        public class WerkstattVerwaltung
        {
        MySqlConnection connection;
        public WerkstattVerwaltung()
        {

        }
        public WerkstattVerwaltung(MySqlConnection c)
        {
            this.connection = c;
            if (c.State != System.Data.ConnectionState.Open)
            {
                c.Open();
            }
            
        }


        public List<auto> GetAll()
        {
            List<auto> list = new List<auto>();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto";
                command.ExecuteNonQuery();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) });
                    }
                }
            }
            return list;
        }

        public async Task<List<auto>> GetAllAsync()
        {
            List<auto> list = new ();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto";
                await command.ExecuteNonQueryAsync();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        list.Add(new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) });
                    }
                }
            }
            return list;
        }


        public auto GetOne(int id)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = id;

                command.ExecuteNonQuery();

                using (var reader = command.ExecuteReader())
                    while (reader.Read())
                    {
                        auto a = new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) };
                        return a;
                    }
                return null;
            }
        }

        public async Task<auto> GetOneAsync(int id)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = id;

                await command.ExecuteNonQueryAsync();

                using (var reader = await command.ExecuteReaderAsync())
                    while (reader.Read())
                    {
                        auto a = new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) };
                        return a;
                    }
                return null;
            }
        }

        public void EditByID(auto a)
        {
            GetOne(a.id);
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE auto SET marke = @marke, modell = @modell WHERE id = @_id";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = a.id;

                command.ExecuteNonQuery();
            }
        }

        public async Task EditByIDAsync(auto a)
        {
            await Task.Run(() => GetOneAsync(a.id));
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE auto SET marke = @marke, modell = @modell WHERE id = @_id";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = a.id;

                await command.ExecuteNonQueryAsync();
            }
        }


        public void Delete(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.VarChar).Value = a.id;
                command.ExecuteNonQuery();
            }
        }
        public async Task DeleteAsync(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.VarChar).Value = a.id;
                await command.ExecuteNonQueryAsync();
            }
        }

        public void Insert(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO auto(marke, modell) VALUES(@marke, @modell)";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;

                command.ExecuteNonQuery();
            }
        }

        public async Task InsertAsync(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO auto(marke, modell) VALUES(@marke, @modell)";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;

                await command.ExecuteNonQueryAsync();
            }
        }
    }

    public class auto
    {
        public int id { get; set; }
        public string marke { get; set; }
        public string modell { get; set; }

        public override string ToString()
        {
            return $"{id} - {marke} / {modell}";
        }
    }

    public class GUIConsole
    {
        WerkstattVerwaltung WV;
        public GUIConsole(WerkstattVerwaltung WV)
        {
            this.WV = WV;
        }


        public void mainLoop()
        {
            while (true)
            {
                Console.Clear();
                ListAll();
                Console.WriteLine("\ne X => Edit id X \nd X => Delete id X \ni => Insert \n");
                string input = Console.ReadLine();

                Int32.TryParse(input?.Substring(2), out int nr);

                switch (input[0])
                {
                    case 'e': EditAuto(nr); break;
                    case 'd': DeleteAuto(nr); break;
                    case 'i': newAuto(); break;
                    default: ListAll(); break;
                }

            }
        }

        void ListAll()
        {
            List<auto> werkstatt = WV.GetAll();
            foreach (var item in werkstatt)
            {
                Console.WriteLine($"{item.id}, {item.marke}, {item.modell}");
            }
        }
        void DeleteAuto(int id)
        {
            WV.Delete(new auto { id = id });
        }
        void EditAuto(int id)
        {
            auto a = new auto();
            a.id = id;
            Console.Write("neue Marke: ");
            a.marke = Console.ReadLine();
            Console.Write("neues Modell: ");
            a.modell = Console.ReadLine();

            WV.EditByID(a);
        }
        void newAuto()
        {
            auto a = new auto();
            Console.Write("Marke: ");
            a.marke = Console.ReadLine();
            Console.Write("Modell: ");
            a.modell = Console.ReadLine();
            WV.Insert(a);
        }

    }
}