﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;

namespace DependecyInjectionAufgabe4
{
    internal class WerkstattVerwaltung
    {
        MySqlConnection connection;
        public WerkstattVerwaltung()
        {

        }
        public WerkstattVerwaltung(MySqlConnection c)
        {
            this.connection = c;
            connection.Open();
        }
        public List<auto> GetAll()
        {
            List<auto> list = new List<auto>();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto";
                command.ExecuteNonQuery();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) });
                    }
                }
            }
            return list;


        }

        public auto GetOne(int id)
        {

            using (var command = connection.CreateCommand())
            {
                command.CommandText = "SELECT  * FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = id;

                command.ExecuteNonQuery();

                using (var reader = command.ExecuteReader())
                    while (reader.Read())
                    {
                        auto a = new auto { id = reader.GetInt32(0), marke = reader.GetString(1), modell = reader.GetString(2) };
                        return a;
                    }
                return null;

            }

        }
        public void EditByID(auto a)
        {
            GetOne(a.id);


            using (var command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE auto SET marke = @marke, modell = @modell WHERE id = @_id";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;
                command.Parameters.Add("@_id", MySqlDbType.Int32).Value = a.id;

                command.ExecuteNonQuery();
            }

        }
        public void Delete(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM auto WHERE id = @_id";
                command.Parameters.Add("@_id", MySqlDbType.VarChar).Value = a.id;
                command.ExecuteNonQuery();
            }
        }

        public void Insert(auto a)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO auto(marke, modell) VALUES(@marke, @modell)";
                command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = a.marke;
                command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = a.modell;

                command.ExecuteNonQuery();
            }
        }
    }

    internal class auto
    {
        public int id { get; set; }
        public string marke { get; set; }
        public string modell { get; set; }
    }

    internal class GUIConsole
    {
        WerkstattVerwaltung WV;
        public GUIConsole(WerkstattVerwaltung WV)
        {
            this.WV = WV;
        }


        public void mainLoop()
        {
            while (true)
            {
                Console.Clear();
                ListAll();
                Console.WriteLine("\ne X => Edit id X \nd X => Delete id X \ni => Insert \n");
                string input = Console.ReadLine();

                Int32.TryParse(input?.Substring(2), out int nr);

                switch (input[0])
                {
                    case 'e': EditAuto(nr); break;
                    case 'd': DeleteAuto(nr); break;
                    case 'i': newAuto(); break;
                    default: ListAll(); break;
                }

            }
        }

        void ListAll()
        {
            List<auto> werkstatt = WV.GetAll();
            foreach (var item in werkstatt)
            {
                Console.WriteLine($"{item.id}, {item.marke}, {item.modell}");
            }
        }
        void DeleteAuto(int id)
        {
            WV.Delete(new auto { id = id });
        }
        void EditAuto(int id)
        {
            auto a = new auto();
            a.id = id;
            Console.Write("neue Marke: ");
            a.marke = Console.ReadLine();
            Console.Write("neues Modell: ");
            a.modell = Console.ReadLine();

            WV.EditByID(a);
        }
        void newAuto()
        {
            auto a = new auto();
            Console.Write("Marke: ");
            a.marke = Console.ReadLine();
            Console.Write("Modell: ");
            a.modell = Console.ReadLine();
            WV.Insert(a);
        }

    }
}

