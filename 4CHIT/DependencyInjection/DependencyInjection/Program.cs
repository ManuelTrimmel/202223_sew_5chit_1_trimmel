﻿// See https://aka.ms/new-console-template for more information
using MySqlConnector;
using DependencyInjection;




MySqlConnection c = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt");
WerkstattVerwaltung WV = new WerkstattVerwaltung(c);

while (true)
{
    Console.Clear();
    WV.GetAll();
    Console.WriteLine("\ne X => Edit id X \nd X => Delete id X \ni => Insert \n");
    string input = Console.ReadLine();

    Int32.TryParse(input?.Substring(2), out int nr);

    switch (input[0])
    {
        case 'e':WV.EditByID(nr); break;
        case 'd':WV.Delete(nr); break;
        case 'i':WV.Insert();break;
        default: WV.GetAll();break;
    }

}






