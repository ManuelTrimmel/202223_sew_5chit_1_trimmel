﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
namespace DependencyInjection
{
    internal class WerkstattVerwaltung
    {
        MySqlConnection connection;
        public WerkstattVerwaltung()
        {

        }
        public WerkstattVerwaltung(MySqlConnection c)
        {
            this.connection = c;
            connection.Open();
        }
        public void GetAll()
        {



                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT  * FROM auto";

                    command.ExecuteNonQuery();

                    using (var reader = command.ExecuteReader())
                        while (reader.Read())
                            Console.WriteLine($"{reader.GetInt32(0)}, {reader.GetString(1)}, {reader.GetString(2)}");
                }
            

        }

        public void GetOne(int id)
        {

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT  * FROM auto WHERE id = @_id";
                    command.Parameters.Add("@_id", MySqlDbType.Int32).Value = id;

                    command.ExecuteNonQuery();

                    using (var reader = command.ExecuteReader())
                        while (reader.Read())
                            Console.WriteLine($"{reader.GetInt32(0)}, {reader.GetString(1)}, {reader.GetString(2)}");
                }
            
        }
        public void EditByID(int id)
        {
            GetOne(id);

            Console.Write("neue Marke: ");
            string marke = Console.ReadLine();
            Console.Write("neues Modell: ");
            string modell = Console.ReadLine();

            using (var connection = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE auto SET marke = @marke, modell = @modell WHERE id = @_id";
                    command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = marke;
                    command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = modell;
                    command.Parameters.Add("@_id", MySqlDbType.Int32).Value = id;

                    command.ExecuteNonQuery();
                }



            }
        }
        public void Delete(int id)
        {

            using (var connection = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM auto WHERE id = @_id";
                    command.Parameters.Add("@_id", MySqlDbType.VarChar).Value = id;
                    command.ExecuteNonQuery();
                }
            }
        }


        public void Insert()
        {
            Console.WriteLine();
            Console.Write("Marke: ");
            string marke = Console.ReadLine();
            Console.Write("Modell: ");
            string modell = Console.ReadLine();

            using (var connection = new MySqlConnection("Server=localhost;User ID=root;Password=;Database=db_werkstatt"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "INSERT INTO auto(marke, modell) VALUES(@marke, @modell)";
                    command.Parameters.Add("@marke", MySqlDbType.VarChar).Value = marke;
                    command.Parameters.Add("@modell", MySqlDbType.VarChar).Value = modell;

                    command.ExecuteNonQuery();
                }



            }
        }
    }
}
