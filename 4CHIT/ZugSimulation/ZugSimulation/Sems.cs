﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZugSimulation
{
    class Sems
    {
        public static Semaphore Sem1, Sem2, Sem3;
        static Sems()
        {
            Sem1 = new Semaphore(0, 1);
            Sem2 = new Semaphore(0, 1);
            Sem3 = new Semaphore(0, 1);
        }
    }
}
