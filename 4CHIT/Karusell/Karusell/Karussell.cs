﻿namespace Karusell
{
    public class Karussell
    {
        public void Run()
        {
            while (true)
            {
                Sems.Station.WaitOne();
                for (int i = 0; i < Sems.Sitzplatz; i++)
                {
                    Sems.karvoll.WaitOne();
                }
                Abheben();
                Thread.Sleep(100);
                Runterkommen();
                Sems.ontrack.Release(5);
                Sems.ticket.Release(5);
                Sems.Station.Release();
            }
        }

        private void Abheben()
        {
            //Console.WriteLine("Leaving station.");
            Thread.Sleep(200);
        }

        public void Runterkommen()
        {
            //Console.WriteLine("Entering station");
            Thread.Sleep(100);
        }

    }
}
