﻿namespace Karusell
{
    static class Sems
    {
        public static int Sitzplatz = 5;

        public static Semaphore Station = new Semaphore(5, Sitzplatz);
        public static Semaphore karvoll = new Semaphore(0, 5);
        public static Semaphore ontrack = new Semaphore(0, 5);
        public static Semaphore ticket = new Semaphore(5, 5);

    }

    public class Customer
    {
        public void Run()
        {
            BuyTicket();
            Sems.ticket.WaitOne();
            EnterKarussell();

            Sems.karvoll.Release();
            Sems.ontrack.WaitOne();
            //Sems.ontrack.WaitOne();
            LeaveKarussell();
        }

        private void BuyTicket()
        {
            //Console.WriteLine("Buying ticket.");
            Thread.Sleep(200);
        }

        public void EnterKarussell()
        {
            //Console.WriteLine("Entering train");
            Thread.Sleep(100);
        }

        public void LeaveKarussell()
        {
            //Console.WriteLine("Leaving train");
            Thread.Sleep(100);
        }

    }
}
