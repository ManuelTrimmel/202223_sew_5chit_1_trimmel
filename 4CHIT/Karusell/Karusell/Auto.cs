﻿namespace Karusell
{
    public enum Direction { LEFT, DOWN, RIGHT, UP }
    public class Auto
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int H { get; private set; }
        public int W { get; private set; }

        public Auto(int x, int y, int h, int w)
        {
            X = x;
            Y = y;
            H = h;
            W = w;

        }
        public string Move(int step, Direction which)
        {
            if (which == Direction.UP) Y -= step;
            if (which == Direction.RIGHT) X += step;
            if (Y < 20) //reappear at random position
                Respawn(Direction.UP);
            if (X > 400) //reappear at random position
                Respawn(Direction.RIGHT);
            return "";
        }
        public void Respawn(Direction which)
        {
            if (which == Direction.UP) Y = 400;
            if (which == Direction.RIGHT) X = 0;
        }
    }
}
