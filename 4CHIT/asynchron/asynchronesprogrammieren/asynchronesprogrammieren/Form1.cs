﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace asynchronesprogrammieren
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tuwas();
        }

        public void Tuwas()
        {
            System.Threading.Thread.Sleep(5000);
            MessageBox.Show("Tuwas fiade!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Thread(Tuwas).Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           Machwas();
        }

        async void Machwas()
        {
            await Task.Delay(5000);
            MessageBox.Show("Machwas fiade");
        }


        public async Task<int> Lottozahl()
        {
            await Task.Delay(new Random().Next(2000, 5000));
            return new Random().Next(1, 45);
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            var i = await Lottozahl();

            MessageBox.Show(i.ToString());
        }

        public async Task<int> RandomGenerator()
        {
            Random R = new Random();
            await Task.Delay(100);
            return R.Next(10, 200);
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void button5_Click(object sender, EventArgs e)
        {
            var zahl = await RandomGenerator();
            MessageBox.Show(zahl.ToString());
            
        }
    }




}
