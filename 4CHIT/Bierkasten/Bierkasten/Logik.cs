﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Bierkasten;

namespace Bierkasten
{
    class Sems
    {
        public static Semaphore _supplier, _filler, _transporter;
        static Sems()
        {
            _supplier = new Semaphore(0, 1);
            _filler = new Semaphore(0, 1);
            _transporter = new Semaphore(0, 1);
        }
    }
    abstract class Worker
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public int progress = 0;

        public void DoWork()
        {

        }



    }

        

       
    

    class Filler : Worker
    {
        public string FillStatus = "";
        

        public void FillBox()
        {
            while (true)
            {
                // Wait for clearance

                Sems._filler.WaitOne();
                while (true)
                {
                    // Fill Box
                    progress++;
                    if (progress % 5 == 0)
                    {
                        FillStatus += "O";
                    }

                    // calculate status color


                    Thread.Sleep(50);

                    if (progress >= 100)
                    {
                        progress = 0;
                        FillStatus = "";
                        Sems._supplier.Release();
                        Sems._transporter.Release();
                        break;
                    }
                }
            }
        }




    }
}
