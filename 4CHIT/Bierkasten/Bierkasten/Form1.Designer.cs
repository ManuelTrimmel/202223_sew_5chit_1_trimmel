﻿
namespace Bierkasten
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bierkastn = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.Refiller = new System.Windows.Forms.Label();
            this.Transporter = new System.Windows.Forms.Label();
            this.supplier = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bierkastn
            // 
            this.bierkastn.Location = new System.Drawing.Point(50, 84);
            this.bierkastn.Name = "bierkastn";
            this.bierkastn.Size = new System.Drawing.Size(35, 62);
            this.bierkastn.TabIndex = 0;
            this.bierkastn.Text = "oooooooooooooooo";
            this.bierkastn.Click += new System.EventHandler(this.bierkastn_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(96, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Refiller
            // 
            this.Refiller.AutoSize = true;
            this.Refiller.Location = new System.Drawing.Point(50, 55);
            this.Refiller.Name = "Refiller";
            this.Refiller.Size = new System.Drawing.Size(43, 15);
            this.Refiller.TabIndex = 2;
            this.Refiller.Text = "Refiller";
            // 
            // Transporter
            // 
            this.Transporter.AutoSize = true;
            this.Transporter.Location = new System.Drawing.Point(406, 165);
            this.Transporter.Name = "Transporter";
            this.Transporter.Size = new System.Drawing.Size(66, 15);
            this.Transporter.TabIndex = 3;
            this.Transporter.Text = "Transporter";
            // 
            // supplier
            // 
            this.supplier.AutoSize = true;
            this.supplier.Location = new System.Drawing.Point(76, 326);
            this.supplier.Name = "supplier";
            this.supplier.Size = new System.Drawing.Size(50, 15);
            this.supplier.TabIndex = 4;
            this.supplier.Text = "Supplier";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.supplier);
            this.Controls.Add(this.Transporter);
            this.Controls.Add(this.Refiller);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bierkastn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label bierkastn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Refiller;
        private System.Windows.Forms.Label Transporter;
        private System.Windows.Forms.Label supplier;
    }
}

