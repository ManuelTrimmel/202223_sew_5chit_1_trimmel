﻿using System;
using System.Collections.Generic;

namespace Model
{
    public partial class TSchueler
    {
        public int Id { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public DateTime Geburtsdatum { get; set; }
    }
}
