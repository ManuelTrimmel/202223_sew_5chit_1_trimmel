﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Model
{
    public partial class db_sewContext : DbContext
    {
        public db_sewContext()
        {
        }

        public db_sewContext(DbContextOptions<db_sewContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TSchueler> TSchuelers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;database=db_sew;user=root;treattinyasboolean=true", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.4.19-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_general_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<TSchueler>(entity =>
            {
                entity.ToTable("t_schueler");

                entity.Property(e => e.Id)
                    .HasColumnType("int(16)")
                    .HasColumnName("id");

                entity.Property(e => e.Geburtsdatum).HasColumnName("geburtsdatum");

                entity.Property(e => e.Nachname)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("nachname");

                entity.Property(e => e.Vorname)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("vorname");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
