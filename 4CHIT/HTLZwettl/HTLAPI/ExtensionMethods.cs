﻿namespace HTLAPI
{

    public static class ExtensionMethod
    {
        public static object GetPropValue(object src, string propName)
        {
            object? o = null;
            try
            {
                o = src.GetType().GetProperty(propName).GetValue(src, null);
                return o;
            }
            catch (Exception e)
            {
                return o;
            }
        }
    }
}

