﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Model;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HTLAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchuelerController : ControllerBase
    {
        db_sewContext context = new db_sewContext();
        [EnableCors()]
        // GET: api/<SchuelerController>
        [HttpGet]
        public IEnumerable<TSchueler> Get()
        {
            return context.TSchuelers;
        }

        // GET api/<SchuelerController>/5
        [EnableCors()]
        [HttpGet("{id}")]
        public TSchueler Get(int id)
        {
            return context.TSchuelers.Where(i => i.Id == id).FirstOrDefault();
        }

        // POST api/<SchuelerController>
        [HttpPost]
        public void Post([FromBody] TSchueler value)
        {
            context.TSchuelers.Add(value);
            context.SaveChanges();
        }

        // PUT api/<SchuelerController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TSchueler value)
        {
            //if (context.TSchuelers.Contains(value) == true)
            //{
            //    context.TSchuelers.Update(value);
            //}
            //context.SaveChanges();
            TSchueler tmp = Get(id);
            Delete(id);
            Post(value);

        }

        // DELETE api/<SchuelerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                context.TSchuelers.Remove(Get(id));
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("geht ned");
            }
        }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T> : ControllerBase where T: class
    {
        db_sewContext context = new db_sewContext();
        [EnableCors()]
        // GET: api/<SchuelerController>
        [HttpGet]
        public IEnumerable<T> Get()
        {
            var entity = context.Set<T>();
            return entity;
        }

        // GET api/<SchuelerController>/5
        [EnableCors()]
        [HttpGet("{id}")]
        public T Get(int id)
        {
            var entity = context.Set<T>().Find(id);
            return entity;
        }

        // POST api/<SchuelerController>
        [HttpPost]
        public void Post([FromBody] T value)
        {
            context.Set<T>().Add(value);
            context.SaveChanges();
        }

        // PUT api/<SchuelerController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] T value)
        {
            //if (context.TSchuelers.Contains(value) == true)
            //{
            //    context.TSchuelers.Update(value);
            //}
            //context.SaveChanges();
            
            //Delete(id);
            //Post(value);

            if (context.Set<T>().Contains(value) == true)
            {
                context.Set<T>().Update(value);
            }
            context.SaveChanges();

        }

        // DELETE api/<SchuelerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                context.Set<T>().Remove(Get(id));
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("geht ned");
            }
        }
    }
    public class PupilController : GenericController<TSchueler>
    {
    }
}
