﻿using System;
using System.Threading;
namespace Multithreading
{
    class Program
    {
        
        static void Main(string[] args)
        {
            ThreadStart ts = new ThreadStart(Machwas);
            Thread t = new Thread(ts);
            t.Start();
            new Thread((x) => Machwas()).Start(2);



            ParameterizedThreadStart ts2 = new ParameterizedThreadStart(Machwas2);
            Thread t2 = new Thread(ts2);
            t2.Start();
            new Thread((x) => Machwas2(5)).Start();
        }

        public static void Machwas()
        {
            Console.WriteLine("i bins");
            //nix do
        }

        public static void Machwas2(object o)
        {
            Console.WriteLine((int)o);
        }
    }
}
