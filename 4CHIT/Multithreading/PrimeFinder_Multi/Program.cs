﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace PrimeFinder_Multi
{
    //Zusammenarbeit von Manuel Trimmel und Andreas Nechwachtal
    class AmazonPrime
    {
        public int paket;
        public int[] result = new int[1000];
    }

    class Program
    {
        public static int count = 0;
        static void Main(string[] args)
        {
            

            //List<int> primes = findPrimes(1000);
            //AmazonPrime AP = (AmazonPrime)o;

            //ThreadStart ts = new ThreadStart(findPrimes());

            Thread t = new Thread(() => findPrimes(1000));
            Thread t2 = new Thread(() => findPrimes2(1000));

            Stopwatch sw1 = new Stopwatch();
            Stopwatch sw2 = new Stopwatch();

            //sw1.Start();
            //t.Start();
            //sw1.Stop();

            sw2.Start();
            t2.Start();
            sw2.Stop();
            //Console.WriteLine(sw1.Elapsed.TotalMilliseconds);
            Console.WriteLine(sw2.Elapsed.TotalMilliseconds);

            Console.WriteLine(count);
            //sw1.Start();
            //t.Start();
            //t2.Start();
            //sw1.Stop();
            //Console.WriteLine(sw1.Elapsed.TotalMilliseconds);

            //t.Start();
            //new Thread((o) => Console.WriteLine((int)o * (int)o)).Start(3);

            //foreach (int prime in primes)
            //{
            //    Console.Write(prime.ToString() + "; ");
            //}
            //Console.ReadKey();
        }

        public static void findPrimes(int n)
        {
            n = n / 2;
            bool[] prime = new bool[n];
            //Zuerst alle Zahlen von 2 bis n als Primzahl markieren
            for (int i = 2; i < n; i++)
            {
                prime[i] = true;
            }

            //Einzelner Abschnitt
            {
                //Wir wollen bei 2 anfangen
                int i = 2;

                //Alle Produkte des Teilers i
                //angefangen bei 2, bis kleiner n durchlaufen
                //Wenn n = 50, dann ist bei i = 7 Schluss, weil das Produkt = 49 ist
                for (; i * i < n; i++)
                {
                    //Wenn die Zahl im Array als Primzahl markiert ist
                    //Was bei den ersten beiden 2 und 3 definitiv der Fall ist
                    if (prime[i])
                    {
                        count++;
                        //Primzahl bis Wurzel(n) ausgeben
                        //Console.WriteLine(i);
                        //Alle weiteren Produkte des Teilers i
                        //angefangen beim Produkt i * i bis kleiner n durchlaufen
                        //j wird mit i beim nächsten Durchlauf (Vielfaches) addiert
                        for (int j = i * i; j < n; j += i)
                        {
                            //Dies kann unmöglich eine Primzahl sein
                            //weil es ein Vielfaches von i ist.
                            prime[j] = false;
                        }
                    }
                }

                //Alle Primzahlen ausgeben
                for (; i < n; i++)
                {
                    if (prime[i])
                    {
                        count++;
                        //Console.WriteLine(i);
                    }
                }
            }
        }

        public static void findPrimes2(int n)
        {
            
            bool[] prime = new bool[n];
            //Zuerst alle Zahlen von 2 bis n als Primzahl markieren
            for (int i = n/2+1; i < n; i++)
            {
                prime[i] = true;
            }

            //Einzelner Abschnitt
            {
                //Wir wollen bei 2 anfangen
                int i = n/2 +1;

                //Alle Produkte des Teilers i
                //angefangen bei 2, bis kleiner n durchlaufen
                //Wenn n = 50, dann ist bei i = 7 Schluss, weil das Produkt = 49 ist
                for (; i * i < n; i++)
                {
                    //Wenn die Zahl im Array als Primzahl markiert ist
                    //Was bei den ersten beiden 2 und 3 definitiv der Fall ist
                    if (prime[i])
                    {
                        
                        //Primzahl bis Wurzel(n) ausgeben
                        //Console.WriteLine(i);
                        //Alle weiteren Produkte des Teilers i
                        //angefangen beim Produkt i * i bis kleiner n durchlaufen
                        //j wird mit i beim nächsten Durchlauf (Vielfaches) addiert
                        for (int j = i * i; j < n; j += i)
                        {
                            //Dies kann unmöglich eine Primzahl sein
                            //weil es ein Vielfaches von i ist.
                            prime[j] = false;
                        }
                    }
                }

               // Alle Primzahlen ausgeben
                for (; i < n; i++)
                {
                    if (prime[i])
                    {
                        count++;
                        //Console.WriteLine(i);
                    }
                }
            }
        }
    



    }
}
