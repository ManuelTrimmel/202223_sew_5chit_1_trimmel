﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace PONG
{
    class Program
    {
        static void Main(string[] args)
        {
            Example ex = new Example();

            //Thread p = new Thread(new ThreadStart(ex.Ping));
            //Thread c = new Thread(new ThreadStart(ex.Pong));

            //Sems._ping.Release();

            //p.Start();
            //c.Start();
            //Console.ReadKey();
            ParameterizedThreadStart ts = new ParameterizedThreadStart(ex.Ponk);
            ParameterizedThreadStart ts2 = new ParameterizedThreadStart(ex.Pink);

            Sems._ping.Release();
            new Thread(ts).Start(50);
            new Thread(ts2).Start(50);    

            //new Thread(new ThreadStart(ex.Pong)).Start();
            //Sems._ping.Release();
        }
    }


    class Sems
    {
        public static Semaphore _ping, _pong;

        static Sems()
        {
            _ping = new Semaphore(0, 1);
            _pong = new Semaphore(0, 1);
        }
    }

    class Example
    {
        int i = 20;
        public void Ping()
        {
            while(i>0)
            {
                Sems._ping.WaitOne();
                Console.WriteLine("Ping = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString()) ;
                i--;

                Sems._pong.Release();
            }
        }

        public void Pong()
        {
            int i = 20;

            while(i>0)
            { 
            Sems._pong.WaitOne();
            Console.WriteLine("Pong = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString());
            i--;

            Sems._ping.Release();
            }
        }

        public void Ponk(object _i)
        {
            int i = (int)_i;
            while (i > 0)
            {
                Sems._pong.WaitOne();
                Console.WriteLine("Pong = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString());
                i--;

                Sems._ping.Release();
            }
        }
        public void Pink(object _i)
        {
            int i = (int)_i;
            while (i > 0)
            {
                Sems._ping.WaitOne();
                Console.WriteLine("Ping = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString());
                i--;

                Sems._pong.Release();
            }
        }
    }

}



