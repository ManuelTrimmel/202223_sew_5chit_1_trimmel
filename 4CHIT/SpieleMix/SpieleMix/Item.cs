﻿using static SpieleMix.Pages.FetchData;

namespace SpieleMix
{
    public class Item : Mama
    {
        public int count = 0;
        public int R;
        public Item(int X, int Y, int R)
        {
            base.X = X;
            base.Y = Y;
            this.R = R;
        }
        public override void Move(Direction which)
        {
            if (which == Direction.DOWN) Y++;
            if (Y > 300) //reappear at random position
            {
                Spawn();
            }
        }
        public void Collision(PacMan mampf)
        {
            if ((mampf.X - 3 < this.X && mampf.X + 53 > this.X) && (mampf.Y - 3 < this.Y && mampf.Y + 53 > this.Y))
            {
                Spawn();
            }
        }
        public void Spawn()
        {
            Random r = new Random();
            count++;
            Y = 30;
            X = r.Next(20, 720);
        }
    }
}
