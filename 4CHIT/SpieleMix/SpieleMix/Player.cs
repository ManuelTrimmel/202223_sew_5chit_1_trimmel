﻿using static SpieleMix.Pages.FetchData;

namespace SpieleMix
{
    public class Player : Mama 
    {
        public int W;
        public int H;
        public Player(int X, int Y, int W, int H)
        {
            this.X = base.X;
            this.Y = base.Y;
            this.W = W;
            this.H = H;
        }

        public override void Move(Direction which)
        {
            if (which == Direction.LEFT) X -= step;
            if (which == Direction.RIGHT) X += step;
        }
        public void SetX(int x)
        {
            X = x - W / 2;
        }
        public void SetY(int y)
        {
            Y = y;
        }
    }
}
