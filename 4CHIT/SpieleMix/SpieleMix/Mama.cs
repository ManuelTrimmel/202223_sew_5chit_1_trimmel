﻿using static SpieleMix.Pages.FetchData;

namespace SpieleMix
{
    public abstract class Mama
    {
        public int X { get; protected set; }
        public int Y { get; protected set; }
        public abstract void Move(Direction which);
       
    }
}