﻿using static SpieleMix.Pages.FetchData;

namespace SpieleMix
{
    public class PacMan : Mama
    {
        public PacMan(int X, int Y)
        {
            this.Y = base.Y;
            this.X = X;
        }
        public override void Move(Direction which)
        {
            if (which == Direction.LEFT)
            {
                if (X > 0) X -= step;
            
            }

            if (which == Direction.RIGHT)
            {
                if (X < 750) X += step;
           
            }
            if (which == Direction.UP)
            {
                if (Y > 0) Y -= step;
              
            
            }
            if (which == Direction.DOWN)
            {
                if (Y < 250) Y += step;
                

            }
            
        }
    }
}
