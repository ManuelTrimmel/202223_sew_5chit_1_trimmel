﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_Logik
{
    public partial class Form1 : Form
    {
        bool leben = true;
        Point neuekopfposition;
        snake_controller sc = new snake_controller();

        public Color color = Color.Green;
        private Point richtung = new Point(10, 0);
        private Random random = new Random();
        static int fensterbreite = 300;
        static int fensterhoehe = 300;

        private Point apfel = new Point();

        public int score = 1;

        private int snakeSpeed = 10;

        private List<Point> snake = new List<Point>()
        {
            new Point(fensterhoehe/2, fensterbreite/2)
        };

        private void GameLoop(object sender, System.EventArgs e)
        {
            if (leben == true)
            {
                Update();
                Invalidate();
            }

        }

        public new void Update()
        {
            if (leben == true)
            {
                neuekopfposition = new Point(
                snake[0].X + richtung.X,
                snake[0].Y + richtung.Y
                );
            }


        


            snake.Insert(0, neuekopfposition);
            snake.RemoveAt(snake.Count - 1);




            //Schlange kann Apfel essen
            if (snake[0].X == apfel.X && snake[0].Y == apfel.Y)
            {
                snake.Add(new Point(apfel.X -1, apfel.Y -1));   
                GenerateApfel();
                label2.Text = "Score: " + score++;
            }
            SchlangeKopfnuss();
           
        }

        
        public Form1()
        {
            
            InitializeComponent();
            this.DoubleBuffered = false;

            ClientSize = new Size(fensterbreite, fensterhoehe);

            var timer = new Timer();
            timer.Tick += GameLoop;
            timer.Interval = 150;
            timer.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GenerateApfel();

        }

        private void DrawRect(int x, int y, Color color, int size = 10)
        {
            Graphics g = CreateGraphics();
            SolidBrush brush = new SolidBrush(color);
            g.FillRectangle(brush, new Rectangle(x, y, size, size));
            brush.Dispose();
            g.Dispose();



        }
        private void DrawSnake()
        {
            foreach (var teile in snake)
            {
                DrawRect(teile.X, teile.Y, color);
            }
        }

        private void Form1_Paint_1(object sender, PaintEventArgs e)
        {
            DrawSnake();
            DrawApfel();


            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.Black);
            g.DrawLine(pen, 0, 40 , fensterbreite, 40);

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
          

            

            switch (sc.controller(sender, e.KeyChar))
            {
                case 'w':  
                    richtung.X = 0;
                    richtung.Y = -snakeSpeed;
                    break;
                case 'a':     
                    richtung.X = -snakeSpeed;
                    richtung.Y = 0;
                    break;
                case 's':
                    richtung.X = 0;
                    richtung.Y = snakeSpeed;
                    break;
                case 'd':
                    richtung.X = snakeSpeed;
                    richtung.Y = 0;
                    break;
            }
            label1.Text = "Input-Taste: " + e.KeyChar.ToString();
            
        }

        private void GenerateApfel()
        {
            apfel.X = 10 * random.Next(0, fensterbreite / 10 - 1);
            apfel.Y = 10 * random.Next(0, (fensterhoehe) / 10 - 1) ;
        }

        private void DrawApfel()
        {
            
          

            
            if (apfel.Y >= 40)
            {

                       DrawRect(apfel.X, apfel.Y, Color.Orange);
                    
                
                
            }
            else
            {
                GenerateApfel();
            }
            

        }


        private void SchlangeKopfnuss()
        {
            //if (snake[0].X == fensterbreite)
            //{
            //    neuekopfposition = new Point(
            //    snake[0].X - snake[0].X,
            //    snake[0].Y
            //    );

            //}
            //else if (snake[0].Y >= fensterhoehe)
            //{
            //    neuekopfposition = new Point(
            //    snake[0].X,
            //    snake[0].Y - snake[0].Y);
            //}
            //else
            //{

            //}
            //for (int i = snake.Count - 1; i >= 0; i--)
            //{
            //    for (int j = 1; j < snake.Count; j++)
            //    {
            //        if (snake[i].X == snake[j].X && snake[i].Y == snake[j].Y)
            //        {
            //            // if so we run the die function
            //            MessageBox.Show("Mulm");
            //        }
            //    }
            //}

            if (snake[0].Y == fensterhoehe || snake[0].X == fensterbreite || snake[0].Y == 30|| snake[0].X == -10)
            {
                leben = false;

                MessageBox.Show("Game Over");
                snakeSpeed = 0;
                

                //neuekopfposition = new Point(
                //fensterbreite / 2,
                //fensterhoehe / 2
                //);
                label1.Text = "GAME OVER";

            }

            for (int i = 1; i < snake.Count(); i++)
            {
                if (snake[i].Y  == snake[0].Y && snake[i].X == snake[0].X)
                {
                    leben = false;
                    label1.Text = "GAME OVER";
                    MessageBox.Show("Game Over");
                }
                
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        
    }
}
