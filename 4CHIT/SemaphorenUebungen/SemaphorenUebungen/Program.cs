﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphorenUebungen
{
    static class Sems
    {
        public static int ZugCNT = 5;
        
        public static Semaphore Station= new Semaphore(5, ZugCNT);
        public static Semaphore zugvoll = new Semaphore(0, 5);
        public static Semaphore ontrack = new Semaphore(0, 5);
        public static Semaphore ticket = new Semaphore(5, 5);

    }
    class Program
    {
        static void Main(string[] args)
        {
            Task[] passagiere = new Task[10];
            for (int i = 0; i < passagiere.Length; i++)
            {
                passagiere[i] = new Task(new Customer().Run);
                passagiere[i].Start();
            }

            Task[] train = new Task[5];
            
            
            for (int i = 0; i < train.Length; i++)
            {
                train[i] = new Task(new Train().Run);
                train[i].Start();
            }

            


            // Startetn Sie die Sumlation in der
            // Main Methode.

            // Starten Sie alle Threads die zur
            // Lsung der Aufgabe notwendig sind


        }
    }
    public class Customer
    {
        public void Run()
        {
            BuyTicket();
            Sems.ticket.WaitOne();
            EnterTrain();
            
            Sems.zugvoll.Release();
            Sems.ontrack.WaitOne();
            //Sems.ontrack.WaitOne();
            LeaveTrain();
        }

        private void BuyTicket()
        {
            Console.WriteLine("Buying ticket.");
            Thread.Sleep(200);
        }

        public void EnterTrain()
        {
            Console.WriteLine("Entering train");
            Thread.Sleep(100);
        }

        public void LeaveTrain()
        {
            Console.WriteLine("Leaving train");
            Thread.Sleep(100);
        }

    }
    public class Train
    {

        public void Run()
        {
            while (true) {
                Sems.Station.WaitOne();
                for (int i = 0; i < Sems.ZugCNT; i++)
                {  
                  Sems.zugvoll.WaitOne();
                }
                LeaveStation();

                Thread.Sleep(100);

                EnterStation();
                Sems.ontrack.Release(5);
                Sems.ticket.Release(5);
                
                
                Sems.Station.Release();
            }



        }

        private void LeaveStation()
        {
            Console.WriteLine("Leaving station.");
            Thread.Sleep(200);
        }

        public void EnterStation()
        {
            Console.WriteLine("Entering station");
            Thread.Sleep(100);
        }

    }
}
