﻿using System;
using System.Linq;
using ScottLINQ;
namespace Scott_Schema
{
    class Program
    {
        static void Main(string[] args)
        {
            Scott_Schema1 scott = new Scott_Schema1();


            //Console.WriteLine("Übung 1");

            //var beispiel1 = from d in scott.dept
            //           select d;

            //foreach (var item in beispiel1)
            //{
            //    Console.WriteLine($"{item.dname} {item.loc}");
            //}

            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 2");

            //var beispiel2 = from emp in scott.emp
            //           where emp.deptno == 10
            //           select emp;

            //foreach (var item in beispiel2)
            //{
            //    Console.WriteLine($"{item.ename} {item.job} {item.hiredate}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 3");

            //var beispiel3 = from emp in scott.emp
            //                where emp.job == "CLERK"
            //           select emp;
            //foreach (var item in beispiel3)
            //{
            //    Console.WriteLine($"{item.ename} {item.job} {item.sal}");
            //}


            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 4");
            //var beispiel4 = from emp in scott.emp
            //                where emp.deptno != 10
            //                select emp;
            //foreach (var item in beispiel4)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 5");
            //var beispiel5 = from emp in scott.emp
            //                where emp.sal < emp.comm
            //                select emp;
            //foreach (var item in beispiel5)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}

            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 6");
            //var beispiel6 = from emp in scott.emp
            //                where emp.hiredate == new DateTime(1981, 12, 3)
            //                select emp;
            //foreach (var item in beispiel6)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 7");
            //var beispiel7 = from emp in scott.emp
            //                where emp.sal <= 1250 || emp.sal >= 1600
            //                select emp;
            //foreach (var item in beispiel7)
            //{
            //    Console.WriteLine($"{item.ename} {item.sal}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 8");

            //var beispiel8 = from emp in scott.emp
            //                where emp.job != "MANAGER" && emp.job != "PRESIDENT"
            //                select emp;
            //foreach (var item in beispiel8)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 9");

            //var beispiel9 = from emp in scott.emp
            //                where emp.ename[2] == 'A'
            //                select emp;
            //foreach (var item in beispiel9)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}
            //Console.WriteLine("------------------- ------------------");


            //Console.WriteLine("Übung 10");

            //var beispiel10 = from emp in scott.emp
            //                 where emp.comm != null
            //                select emp;
            //foreach (var item in beispiel10)
            //{
            //    Console.WriteLine($"{item.empno} {item.ename} {item.job}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 11");

            //var beispiel11 = from emp in scott.emp
            //                 orderby emp.comm
            //                 select emp;
            //foreach (var item in beispiel11)
            //{
            //    Console.WriteLine($"{item.empno} {item.ename} {item.job} {item.comm}");
            //}
            //Console.WriteLine("------------------- ------------------");


            //Console.WriteLine("Übung 12");

            //var beispiel12 = from emp in scott.emp
            //                 where emp.job != "MANAGER" && emp.job != "PRESIDENT"
            //                 orderby emp.deptno, emp.hiredate descending
            //                 select emp;
            //foreach (var item in beispiel12)
            //{
            //    Console.WriteLine($"{item.empno} {item.ename} {item.job} {item.hiredate}");
            //}
            //Console.WriteLine("------------------- ------------------");



            //Console.WriteLine("Übung 13");

            //var beispiel13 = from emp in scott.emp
            //                 where emp.ename.Length == 6
            //                 select emp;
            //foreach (var item in beispiel13)
            //{
            //    Console.WriteLine($"{item.ename}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 14");

            //var beispiel14 = from emp in scott.emp
            //                 where emp.deptno == 30
            //                 select new { Arbeiter=emp.ename, Taetigkeit=emp.job};
            //foreach (var item in beispiel14)
            //{
            //    Console.WriteLine($"{item.Arbeiter} - {item.Taetigkeit}");
            //}
            //Console.WriteLine("------------------- ------------------");

            //Console.WriteLine("Übung 16");

            //var beispiel16 = from emp in scott.emp
            //                 select new { Arbeiter = emp.ename, MONTHLY=emp.sal, DAILY=emp.sal/22, HOURLY=((double)((double)(emp.sal/22))/8) };
            //foreach (var item in beispiel16)
            //{
            //    Console.WriteLine($"{item.Arbeiter} - {item.MONTHLY} - {item.DAILY} - {item.HOURLY}");
            //}
            //Console.WriteLine("------------------- ------------------");


            //Console.WriteLine("Übung 17");

            //var beispiel17 = (from emp in scott.emp
            //                  select emp.sal).Sum();

            //    Console.WriteLine($"{beispiel17}");

            //Console.WriteLine("------------------- ------------------");


            //var erg2 = from d in scott.dept
            //          where d.deptno >= 20 //Selektion
            //          orderby d.loc
            //          select new { d.dname, ort=d.loc}; //PROJEKTION

            //foreach (var item in erg2)
            //{
            //    Console.WriteLine($"{item.dname} {item.ort}");
            //}
            //Console.WriteLine("------------------- ------------------");
            //foreach (var item in erg2)
            //{
            //    Console.WriteLine(item);
            //}



            //_______________________________________________________________ ANGABE 2 (Zettel 2) _________________________________________________________


            Scott_Schema1 scott2 = new Scott_Schema1();

            //19
            Console.WriteLine("-------------------Beispiel------------------");
            var beispiel1_1 = (from d in scott2.emp
                             where d.deptno == 30 && d.sal != null
                             select d.ename).Count();

            var beispiel1_2 = (from d in scott2.emp
                             where d.deptno == 30 && d.comm != null
                             select d.ename).Count();


            Console.WriteLine($" {beispiel1_1} {beispiel1_2}");
          
            //20
            Console.WriteLine("-------------------Beispiel 2------------------");
            var beispiel2 = (from d in scott2.emp
                             group d by d.job into g
                             select g).Count();

            Console.WriteLine(beispiel2);
           
            //22

            Console.WriteLine("-------------------Beispiel 3------------------");
            var beispiel3 = (from d in scott2.emp
                             group d by d.mgr into g
                             select g).Count();
            Console.WriteLine(beispiel3);
            //21

            Console.WriteLine("-------------------Beispiel 4------------------");
            var beispiel4 = (from d in scott2.emp
                             group d by d.mgr into g
                             select g).Count();
            Console.WriteLine(beispiel3);

            //22
            Console.WriteLine("-------------------Beispiel 5------------------");
            //Gehalt
            var beispiel5_1 = (from d in scott.emp
                          where d.deptno == 30
                          select d.sal);
            //Provision
            var beispiel5_2 = (from d in scott.emp
                          where d.deptno == 30
                          where d.comm != null
                          select d.comm);

            Console.WriteLine($"{(int)beispiel5_1.Average()} und {(int)beispiel5_1.Sum()} und {(int)beispiel5_1.Count()} \n {(int)beispiel5_2.Average()} und {(int)beispiel5_2.Sum()} und {(int)beispiel5_2.Count()}");

            //23
            Console.WriteLine("-------------------Beispiel 6------------------");
            var beispiel6 = from e in scott2.emp
                            where e.job != "MANAGER" && e.job != "PRESIDENT"
                            group e by e.deptno into deptnu
                            select (deptnu.Key, deptnu.Count());

            foreach (var item in beispiel6)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("-------------------Beispiel 7 / 24------------------");
            var beispiel7 = from e in scott2.emp
                            group e by e.deptno into deptnu
                            select (deptnu.Count());

            foreach (var item in beispiel7)
            {
                Console.WriteLine(item);
            }

        }
    }
}
