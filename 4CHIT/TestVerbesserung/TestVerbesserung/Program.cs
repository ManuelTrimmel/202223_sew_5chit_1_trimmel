﻿Semaphore A = new Semaphore(1,1);
Semaphore B = new Semaphore(0,1);
Semaphore C = new Semaphore(0,1);

bool check = true;

Thread t1 = new Thread(DoSomething1);

Thread t2 = new Thread(DoSomething2);

Thread t3 = new Thread(DoSomething3);


t1.Start();
t2.Start();
t3.Start();



void DoSomething1()
{
    while (true)
    {
        A.WaitOne();
        Console.Write("A");
        if (check == true)
        {
            B.Release();
            check = false;
        }
        else
        {
            C.Release();
            check = true;
        } 
    }

}
void DoSomething2()
{
    while (true)
    {
        B.WaitOne();
        Console.Write("B");
        A.Release();

    }

}
void DoSomething3()
{
    while (true)
    {
        C.WaitOne();
        Console.Write("C");
        A.Release();
    }

}