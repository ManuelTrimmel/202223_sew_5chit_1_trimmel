﻿Task t = Task.Run(DoIt);

new Task(DoIt).Start();

t.Wait();
Console.WriteLine("Task fertig");
//t.Start();


var abc4 = await DoIt2();
var abc = await DoIt123(2000);
var abc2 = await DoIt123(3000);
var abc3 = await DoIt123(100);



Console.WriteLine(abc);
Console.WriteLine(abc2);
Console.WriteLine(abc3);
Console.WriteLine(abc4);

ParameterizedThreadStart pt = new ParameterizedThreadStart(DoIt3);

Thread thread = new Thread(DoIt);
Thread parathread = new Thread(pt);
//thread.Start();
//parathread.Start(200);

//parathread.Join();
Console.WriteLine("Fertig");


var y = await DoIt2();

Console.WriteLine(y.ToString());
Console.ReadKey();

void DoIt()
{
    for (int i = 0; i < 100; i++)
    {
        Console.WriteLine(i);
        i++;
    }
}


async Task<int> DoIt2()
{
    
    Thread.Sleep(1000);
    return 100;
}
async Task<int> DoIt123(int a)
{
    await Task.Delay(a);
    return 500;
}

void DoIt3(object o)
{
    int y = (int)o;
    for (int i = 0; i < y; i++)
    {
        Console.WriteLine(i);
    }
}