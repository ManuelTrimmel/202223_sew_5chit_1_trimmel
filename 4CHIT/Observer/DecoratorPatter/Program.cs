﻿using System;

namespace DecoratorPatter
{
    public interface ISpielfigur
    {
        public void Drohe();
    }

    public class Monster : ISpielfigur
    {
        public void Drohe()
        {
            Console.WriteLine("Grrrrrrrr.");
        }
    }

    public class Dekorierer : ISpielfigur
    {
        private ISpielfigur meineFigur;

        public Dekorierer(ISpielfigur s)
        {
            meineFigur = s;
        }

        public void Drohe()
        {
            meineFigur.Drohe();
        }
    }

    public class HustenDekorierer : Dekorierer
    {
        public HustenDekorierer(ISpielfigur s)
            : base(s)
        { }

        public new void Drohe()
        {
            Console.Write("Hust, hust. ");
            base.Drohe();
        }
    }

    public class SchnupfenDekorierer : Dekorierer
    {
        public SchnupfenDekorierer(ISpielfigur s)
            : base(s)
        { }

        public new void Drohe()
        {
            Console.Write("Schniff. ");
            base.Drohe();
        }
    }

    class Porgram
    {
        public static void Main()
        {
            ISpielfigur meinMonster = new Monster();
            meinMonster.Drohe();

            ISpielfigur meinVerhustetesMonster = new HustenDekorierer(meinMonster);
            meinVerhustetesMonster.Drohe();

            ISpielfigur meinVerschnupftesMonster = new SchnupfenDekorierer(meinMonster);
            meinVerschnupftesMonster.Drohe();

            ISpielfigur meinVerschnupftesVerhustetesMonster = new SchnupfenDekorierer(new HustenDekorierer(meinMonster));
            meinVerschnupftesVerhustetesMonster.Drohe();

            ISpielfigur meinVerhustetesVerschnupftesMonster = new HustenDekorierer(new SchnupfenDekorierer(meinMonster));
            meinVerhustetesVerschnupftesMonster.Drohe();
        }
    }
}
