﻿using System;

namespace DecoraterPattern_Car
{

    public interface IAuto{

        public void Present();

    }

    public class Auto : IAuto
    {
        public string farbe;
        public int PS;

        public void Present()
        {
            Console.Write("This Car is " + farbe + " and has " + PS + " PS");
        }

    }

    public abstract class Dekorierer : IAuto
    {
        private IAuto Auto;

        public Dekorierer(IAuto A)
        {
            Auto = A;
        }

        public virtual void Present()
        {
            Auto.Present();
        }
    }


    public class Oldtimer : Dekorierer
    {
        int Baujahr;

        public Oldtimer(IAuto A, int Baujahr) : base(A) 
        {
            this.Baujahr = Baujahr; 
        }


        public override void Present()
        {
            base.Present();
            Console.Write(", built in " + Baujahr);
        }
    }

    public class Sportwagen : Dekorierer
    {
        int Topspeed;

        public Sportwagen(IAuto A, int Topspeed) : base(A)
        {
            this.Topspeed = Topspeed;
        }


        public override void Present()
        {
            base.Present();
            Console.Write(", top speed " + Topspeed + " km/h");
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            IAuto BMW = new Auto { farbe = "Black", PS = 116 };
            BMW.Present();
            Console.WriteLine();
            IAuto Oldtimer = new Oldtimer(BMW, 1982);
            Oldtimer.Present();
            Console.WriteLine();
            IAuto M3 = new Sportwagen(BMW, 350);
            M3.Present();
            Console.WriteLine();
            IAuto Audi = new Sportwagen(new Auto { farbe = "grün", PS = 250 }, 230);

            Audi.Present();
            Console.WriteLine();
            IAuto oldaudi = new Oldtimer(Audi, 1922);
            oldaudi.Present();

        }
    }
}
