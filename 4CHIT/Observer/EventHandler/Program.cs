﻿using System;
using System.Collections.Generic;

namespace EventsLernen
{
    class Program
    {
        static void Main(string[] args)
        {
            Teacher Macho = new Teacher();
            
            Student Scheidl = new Student("Paul", Macho);
            Student Lorenz = new Student("Lorenz", Macho);

            Lorenz.Listen();
            Macho.Tell();
            Scheidl.Listen();
            Scheidl.Write();
            Macho.Tell();
            
            
            
        }
    }

    class Teacher
    {
        int current = 0;
        List<string> Testfragen = new List<string> { "A", "B", "C", "D" };
        public int State // nr of random joke
        {
            get { return (current++) % Testfragen.Count; }


        }
        public delegate void Message(string msg);
        public event Message Say;
        public Message Write;

        public void Tell()
        {
            string m = Testfragen[State];
            if(Say!=null)
            Say(m);

            if (Write != null)
                Write(m);
            
        }
    }
    class BetterTeacher
    {
        int current = 0;
        List<string> Testfragen = new List<string> { "A", "B", "C", "D" };
        public int State // nr of random joke
        {
            get { return (current++) % Testfragen.Count; }


        }
        public event EventHandler Say;

        public void Tell()
        {
            string m = Testfragen[State];
            if (Say != null)
                Say(this, new MyEventArgs { Message = m});
        }
    }

    class Student
    {
        public string Name { get; set; }
        Teacher t;

        public Student(string name, Teacher t)
        {
            Name = name;
            this.t = t;
        }

        public void Listen()
        {
            t.Say += T_Say;
        }
        public void Sleep()
        {
            t.Say -= T_Say;
        }
        public void Write()
        {
            t.Write += T_Write;
        }


        public string Cry { get; set; }

        private void T_Say(string msg)
        {
            Console.WriteLine(Name + " : hearing " + msg + " .. memo");
        }
        private void T_Write(string msg)
        {
            Console.WriteLine(Name + " : writing " + msg + " ..so a schaß");
        }
    }

    class MyEventArgs : EventArgs
    {
        public string Message { get; set; }
    }

    class BetterStudent
    {
            public string Name { get; set; }
            BetterTeacher t;

            public BetterStudent(string name, BetterTeacher t)
            {
                Name = name;
                this.t = t;
            }

            public void Listen()
            {
                t.Say += T_Say;
            }
            public void Sleep()
            {
                t.Say -= T_Say;
            }



        private void T_Say(object sender, EventArgs e)
        {
            MyEventArgs mea = e as MyEventArgs;
            Console.WriteLine(Name + " : hearing " + mea.Message + " .. memo");
        }
        
    }
}
