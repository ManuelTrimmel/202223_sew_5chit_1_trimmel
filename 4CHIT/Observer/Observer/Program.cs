﻿using System;
using System.Collections.Generic;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)

        {

            JokeTeller paul = new JokeTeller();



            JokeObserver theWorld = new JokeObserver { Name = "theWorld", Laugh = "Hahaha" };

            JokeObserver tobee = new JokeObserver { Name = "toBee", Laugh = "Höhöh" };

            Teacher Macho = new Teacher {};

            Student Scheidl = new Student ("Paul", Macho );
            Student Lorenz = new Student ("Lorenz", Macho );


            paul.Register(theWorld);

            paul.Register(tobee);



            paul.Notify();  // tell us a joke, please 
            paul.Unregister(theWorld);

            paul.Notify();  // tell us a joke, please 

            
            Macho.Tell();


        }
    }

    abstract class Subject
    {
        protected List<Observer> lo = new List<Observer>();
        public int State { get; }
        public void Register(Observer o)
        {
            lo.Add(o);
        }
        public void Unregister(Observer o)
        {
            lo.Remove(o);
        }
        public abstract void Notify();

    }
    abstract class Observer
    {
        public abstract void Update(string msg);
    }
    class JokeTeller : Subject
    {
        List<string> jokes = new List<string> { "A", "B", "C" };
        public int State // nr of random joke
        {
            get { return new Random().Next(jokes.Count); }
        }
        public override void Notify()
        {
            int currentJoke = State; //current joke
            foreach (var item in base.lo)
            {
                item.Update(jokes[currentJoke]);
            }
        }
    }
    class JokeObserver : Observer
    {
        public string Name { get; set; }
        public string Laugh { get; set; }
        public override void Update(string msg)
        {
            Console.WriteLine(Name + " : hearing " + msg + ", so laughing " + Laugh);
        }
    }

    class Teacher
    {
        int current = 0;
        List<string> Testfragen = new List<string> { "A", "B", "C", "D" };
        public int State // nr of random joke
        {
            get { return (current++) % Testfragen.Count; } 
            
            
        }
        public delegate void Message(string msg);
        public event Message Say;

        public void Tell()
        {
            string m = Testfragen[State];
            Say(m);
        }
    }
    class Student
    {
        public string Name { get; set; }
        Teacher t;

        public Student(string name, Teacher t)
        {
            Name = name;
            this.t = t;
        }

        public void Listen()
        {
            t.Say += T_Say;
        }
        public void Sleep()
        {
            t.Say -= T_Say;
        }
        public string Cry { get; set; }

        private void T_Say(string msg)
        {
            Console.WriteLine(Name + " : hearing "+ msg + " .. memo");
        }

    }

}
