﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Fabrikmethode
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizzeria daToni = new Pizzeria();
            Mahlzeit gutesEssen = daToni.MahlzeitLiefern();
            Console.WriteLine(gutesEssen);
            Console.WriteLine("---------------------------------");

            Rostwurstbude brunosImbiss = new Rostwurstbude();
            gutesEssen = brunosImbiss.MahlzeitLiefern();
            Console.WriteLine(gutesEssen);
            Console.WriteLine("---------------------------------");

            Wirtshaus Fraunz = new Wirtshaus();
            gutesEssen = Fraunz.MahlzeitLiefern();
            Console.WriteLine(gutesEssen);
            Console.WriteLine("---------------------------------");

            KebabMann Keskin = new KebabMann();
            gutesEssen = Keskin.MahlzeitLiefern();
            Console.WriteLine(gutesEssen);
        }
    }

    class Mahlzeit
    {
        public string Empfaenger { get; set; }

        public override string ToString()
        {
            return Empfaenger == null ? "" : "für " + Empfaenger;
        }
    }

    class Pizza : Mahlzeit
    {
        public Pizza()
        {
            Console.WriteLine("Pizza gebacken");
        }
        public override string ToString()
        {
            return "Pizza " + base.ToString();
        }
    }
    class Kebab : Mahlzeit
    {
        public Kebab()
        {
            Console.WriteLine("Donerfleisch ich schneiden Chefe");
        }
        public override string ToString()
        {
            return "Kebab " + base.ToString();
        }
    }


    class Rostwurst : Mahlzeit
    {
        string beilage;
        public Rostwurst(string beilage)
        {
            this.beilage = beilage;
            Console.WriteLine("Rostwurst gebreaten ...");
            if (beilage != "")
            {
                Console.WriteLine(".. serviert mit " + beilage);
            }
        }
        public override string ToString()
        {
            return "Rostwurst mit " + beilage + " , moizeit diggi " + base.ToString();
        }
    }
    class Bier: Mahlzeit
    {
        public Bier()
        {
            Console.WriteLine("Bier wird gezapft");
        }
        public override string ToString()
        {
            return "Prost, Chef " + base.ToString();
        }
    }

    abstract class Restaurant
    {
        public string kunde;

        public Mahlzeit mahlzeit;
        public abstract void MahlzeitZubereiten();

        public virtual void BestllungAufnehmen()
        {
            Console.WriteLine("Ihre Bestellung bitte!, und wer sind Sie?");
            kunde = Console.ReadLine();
        }
        public void MahlzeitServieren()
        {
            Console.WriteLine("Hier Ihre Mahlzeit. Guten Appetit");
        }

        public Mahlzeit MahlzeitLiefern()
        {
            BestllungAufnehmen();
            MahlzeitZubereiten();
            MahlzeitServieren();
            
            if(kunde != null)
            {
                mahlzeit.Empfaenger = kunde;
            }

            return mahlzeit;
        }
    }
    class Pizzeria : Restaurant
    {
        public override void MahlzeitZubereiten()
        {
            mahlzeit = new Pizza();
        }
        public override void BestllungAufnehmen()
        {
            Console.WriteLine("Ihre Bestellung bitte!, und wer sind Sie?");
            kunde = Console.ReadLine();
        }

    }

    class Rostwurstbude : Restaurant
    {
        public override void MahlzeitZubereiten()
        {
            mahlzeit = new Rostwurst("Majonese");
        }
    }

    class Wirtshaus : Restaurant
    {
        public override void MahlzeitZubereiten()
        {
            mahlzeit = new Bier();
        }
    }

    class KebabMann : Restaurant
    {
        public override void MahlzeitZubereiten()
        {
            mahlzeit = new Kebab();
        }
    }

}
